EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM393 U?
U 1 1 6108D920
P 4800 3400
AR Path="/61044A2C/6108D920" Ref="U?"  Part="1" 
AR Path="/61086D02/6108D920" Ref="U?"  Part="1" 
AR Path="/61422789/6108D920" Ref="U?"  Part="1" 
AR Path="/61422AED/6108D920" Ref="U?"  Part="1" 
AR Path="/61424501/6108D920" Ref="U?"  Part="1" 
AR Path="/6142478E/6108D920" Ref="U?"  Part="1" 
AR Path="/6142B4BA/6108D920" Ref="U?"  Part="1" 
AR Path="/6142D6FB/6108D920" Ref="U?"  Part="1" 
AR Path="/61439586/6108D920" Ref="U?"  Part="1" 
AR Path="/6143C8B2/6108D920" Ref="U12"  Part="1" 
AR Path="/6143F4F0/6108D920" Ref="U?"  Part="1" 
F 0 "U12" H 4800 3767 50  0000 C CNN
F 1 "LM393" H 4800 3676 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 2 1 6143DDDC
P 4800 4700
AR Path="/61044A2C/6143DDDC" Ref="U?"  Part="2" 
AR Path="/61086D02/6143DDDC" Ref="U?"  Part="2" 
AR Path="/61422789/6143DDDC" Ref="U?"  Part="2" 
AR Path="/61422AED/6143DDDC" Ref="U?"  Part="2" 
AR Path="/61424501/6143DDDC" Ref="U?"  Part="2" 
AR Path="/6142478E/6143DDDC" Ref="U?"  Part="2" 
AR Path="/6142B4BA/6143DDDC" Ref="U?"  Part="2" 
AR Path="/6142D6FB/6143DDDC" Ref="U?"  Part="2" 
AR Path="/61439586/6143DDDC" Ref="U?"  Part="2" 
AR Path="/6143C8B2/6143DDDC" Ref="U12"  Part="2" 
AR Path="/6143F4F0/6143DDDC" Ref="U?"  Part="2" 
F 0 "U12" H 4800 5067 50  0000 C CNN
F 1 "LM393" H 4800 4976 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 4700 50  0001 C CNN
	2    4800 4700
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 3 1 6143DDDD
P 4650 1950
AR Path="/61044A2C/6143DDDD" Ref="U?"  Part="3" 
AR Path="/61086D02/6143DDDD" Ref="U?"  Part="3" 
AR Path="/61422789/6143DDDD" Ref="U?"  Part="3" 
AR Path="/61422AED/6143DDDD" Ref="U?"  Part="3" 
AR Path="/61424501/6143DDDD" Ref="U?"  Part="3" 
AR Path="/6142478E/6143DDDD" Ref="U?"  Part="3" 
AR Path="/6142B4BA/6143DDDD" Ref="U?"  Part="3" 
AR Path="/6142D6FB/6143DDDD" Ref="U?"  Part="3" 
AR Path="/61439586/6143DDDD" Ref="U?"  Part="3" 
AR Path="/6143C8B2/6143DDDD" Ref="U12"  Part="3" 
AR Path="/6143F4F0/6143DDDD" Ref="U?"  Part="3" 
F 0 "U12" H 4608 1996 50  0000 L CNN
F 1 "LM393" H 4608 1905 50  0000 L CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4650 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4650 1950 50  0001 C CNN
	3    4650 1950
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 614602E0
P 5150 1500
AR Path="/61044A2C/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602E0" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602E0" Ref="#PWR0143"  Part="1" 
AR Path="/6143F4F0/614602E0" Ref="#PWR?"  Part="1" 
F 0 "#PWR0143" H 5150 1350 50  0001 C CNN
F 1 "+12V" H 5165 1673 50  0000 C CNN
F 2 "" H 5150 1500 50  0001 C CNN
F 3 "" H 5150 1500 50  0001 C CNN
	1    5150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1850 4950 1850
$Comp
L power:GND #PWR?
U 1 1 614602E1
P 4100 1900
AR Path="/61044A2C/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602E1" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602E1" Ref="#PWR0145"  Part="1" 
AR Path="/6143F4F0/614602E1" Ref="#PWR?"  Part="1" 
F 0 "#PWR0145" H 4100 1650 50  0001 C CNN
F 1 "GND" H 4105 1727 50  0000 C CNN
F 2 "" H 4100 1900 50  0001 C CNN
F 3 "" H 4100 1900 50  0001 C CNN
	1    4100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1850 4100 1850
Wire Wire Line
	4100 1850 4100 1900
$Comp
L Device:C C?
U 1 1 614602E8
P 5450 1850
AR Path="/61044A2C/614602E8" Ref="C?"  Part="1" 
AR Path="/61086D02/614602E8" Ref="C?"  Part="1" 
AR Path="/61422789/614602E8" Ref="C?"  Part="1" 
AR Path="/61422AED/614602E8" Ref="C?"  Part="1" 
AR Path="/61424501/614602E8" Ref="C?"  Part="1" 
AR Path="/6142478E/614602E8" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614602E8" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614602E8" Ref="C?"  Part="1" 
AR Path="/61439586/614602E8" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614602E8" Ref="C39"  Part="1" 
AR Path="/6143F4F0/614602E8" Ref="C?"  Part="1" 
F 0 "C39" V 5600 1850 50  0000 C CNN
F 1 "0.1uF" V 5289 1850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5488 1700 50  0001 C CNN
F 3 "~" H 5450 1850 50  0001 C CNN
	1    5450 1850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614602E2
P 5800 1850
AR Path="/61044A2C/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602E2" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602E2" Ref="#PWR0144"  Part="1" 
AR Path="/6143F4F0/614602E2" Ref="#PWR?"  Part="1" 
F 0 "#PWR0144" H 5800 1600 50  0001 C CNN
F 1 "GND" H 5805 1677 50  0000 C CNN
F 2 "" H 5800 1850 50  0001 C CNN
F 3 "" H 5800 1850 50  0001 C CNN
	1    5800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1850 5600 1850
Wire Wire Line
	5300 1850 5150 1850
Connection ~ 5150 1850
$Comp
L power:+12V #PWR?
U 1 1 6143DDEB
P 2500 3850
AR Path="/61044A2C/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDEB" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDEB" Ref="#PWR0149"  Part="1" 
AR Path="/6143F4F0/6143DDEB" Ref="#PWR?"  Part="1" 
F 0 "#PWR0149" H 2500 3700 50  0001 C CNN
F 1 "+12V" H 2515 4023 50  0000 C CNN
F 2 "" H 2500 3850 50  0001 C CNN
F 3 "" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 614602D8
P 3250 4100
AR Path="/61044A2C/614602D8" Ref="R?"  Part="1" 
AR Path="/61086D02/614602D8" Ref="R?"  Part="1" 
AR Path="/61422789/614602D8" Ref="R?"  Part="1" 
AR Path="/61422AED/614602D8" Ref="R?"  Part="1" 
AR Path="/61424501/614602D8" Ref="R?"  Part="1" 
AR Path="/6142478E/614602D8" Ref="R?"  Part="1" 
AR Path="/6142B4BA/614602D8" Ref="R?"  Part="1" 
AR Path="/6142D6FB/614602D8" Ref="R?"  Part="1" 
AR Path="/61439586/614602D8" Ref="R?"  Part="1" 
AR Path="/6143C8B2/614602D8" Ref="R34"  Part="1" 
AR Path="/6143F4F0/614602D8" Ref="R?"  Part="1" 
F 0 "R34" V 3350 4100 50  0000 C CNN
F 1 "50k" V 3250 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 4100 50  0001 C CNN
F 3 "~" H 3250 4100 50  0001 C CNN
	1    3250 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 3850 2500 4100
Wire Wire Line
	2500 4100 3100 4100
Wire Wire Line
	4200 3500 4395 3500
Wire Wire Line
	4500 4600 4200 4600
Wire Wire Line
	5150 1500 5150 1850
Wire Wire Line
	5100 3400 5200 3400
$Comp
L Device:C C?
U 1 1 6143DDE6
P 5200 3700
AR Path="/61044A2C/6143DDE6" Ref="C?"  Part="1" 
AR Path="/61086D02/6143DDE6" Ref="C?"  Part="1" 
AR Path="/61422789/6143DDE6" Ref="C?"  Part="1" 
AR Path="/61422AED/6143DDE6" Ref="C?"  Part="1" 
AR Path="/61424501/6143DDE6" Ref="C?"  Part="1" 
AR Path="/6142478E/6143DDE6" Ref="C?"  Part="1" 
AR Path="/6142B4BA/6143DDE6" Ref="C?"  Part="1" 
AR Path="/6142D6FB/6143DDE6" Ref="C?"  Part="1" 
AR Path="/61439586/6143DDE6" Ref="C?"  Part="1" 
AR Path="/6143C8B2/6143DDE6" Ref="C40"  Part="1" 
AR Path="/6143F4F0/6143DDE6" Ref="C?"  Part="1" 
F 0 "C40" H 5315 3746 50  0000 L CNN
F 1 "15pF" H 5315 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 3550 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
	1    5200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614602ED
P 5200 4250
AR Path="/61044A2C/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602ED" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602ED" Ref="#PWR0151"  Part="1" 
AR Path="/6143F4F0/614602ED" Ref="#PWR?"  Part="1" 
F 0 "#PWR0151" H 5200 4000 50  0001 C CNN
F 1 "GND" H 5205 4077 50  0000 C CNN
F 2 "" H 5200 4250 50  0001 C CNN
F 3 "" H 5200 4250 50  0001 C CNN
	1    5200 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 5200 3400
Wire Wire Line
	5100 4700 5200 4700
$Comp
L Device:C C?
U 1 1 614245CB
P 5200 5000
AR Path="/61044A2C/614245CB" Ref="C?"  Part="1" 
AR Path="/61086D02/614245CB" Ref="C?"  Part="1" 
AR Path="/61422789/614245CB" Ref="C?"  Part="1" 
AR Path="/61422AED/614245CB" Ref="C?"  Part="1" 
AR Path="/61424501/614245CB" Ref="C?"  Part="1" 
AR Path="/6142478E/614245CB" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614245CB" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614245CB" Ref="C?"  Part="1" 
AR Path="/61439586/614245CB" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614245CB" Ref="C42"  Part="1" 
AR Path="/6143F4F0/614245CB" Ref="C?"  Part="1" 
F 0 "C42" H 5315 5046 50  0000 L CNN
F 1 "15pF" H 5315 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 4850 50  0001 C CNN
F 3 "~" H 5200 5000 50  0001 C CNN
	1    5200 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614602EC
P 5200 5500
AR Path="/61044A2C/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602EC" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602EC" Ref="#PWR0156"  Part="1" 
AR Path="/6143F4F0/614602EC" Ref="#PWR?"  Part="1" 
F 0 "#PWR0156" H 5200 5250 50  0001 C CNN
F 1 "GND" H 5205 5327 50  0000 C CNN
F 2 "" H 5200 5500 50  0001 C CNN
F 3 "" H 5200 5500 50  0001 C CNN
	1    5200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4850 5200 4700
Text HLabel 3510 4030 1    50   Input ~ 0
HumIn
$Comp
L Device:R_Potentiometer RV?
U 1 1 6143DDE4
P 3150 3300
AR Path="/61044A2C/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/61086D02/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/61422789/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/61422AED/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/61424501/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/6142478E/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/6142B4BA/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/61439586/6143DDE4" Ref="RV?"  Part="1" 
AR Path="/6143C8B2/6143DDE4" Ref="RV14"  Part="1" 
AR Path="/6143F4F0/6143DDE4" Ref="RV?"  Part="1" 
F 0 "RV14" H 3080 3346 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 3255 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 3300 50  0001 C CNN
F 3 "~" H 3150 3300 50  0001 C CNN
	1    3150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614602DA
P 3150 3650
AR Path="/61044A2C/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602DA" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602DA" Ref="#PWR0148"  Part="1" 
AR Path="/6143F4F0/614602DA" Ref="#PWR?"  Part="1" 
F 0 "#PWR0148" H 3150 3400 50  0001 C CNN
F 1 "GND" H 3155 3477 50  0000 C CNN
F 2 "" H 3150 3650 50  0001 C CNN
F 3 "" H 3150 3650 50  0001 C CNN
	1    3150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 614245D9
P 3150 3000
AR Path="/61044A2C/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614245D9" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614245D9" Ref="#PWR0146"  Part="1" 
AR Path="/6143F4F0/614245D9" Ref="#PWR?"  Part="1" 
F 0 "#PWR0146" H 3150 2850 50  0001 C CNN
F 1 "+12V" H 3165 3173 50  0000 C CNN
F 2 "" H 3150 3000 50  0001 C CNN
F 3 "" H 3150 3000 50  0001 C CNN
	1    3150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3150 3150 3000
Wire Wire Line
	3150 3650 3150 3450
$Comp
L Device:R_Potentiometer RV?
U 1 1 6143DDE1
P 3150 4800
AR Path="/61044A2C/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/61086D02/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/61422789/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/61422AED/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/61424501/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/6142478E/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/6142B4BA/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/61439586/6143DDE1" Ref="RV?"  Part="1" 
AR Path="/6143C8B2/6143DDE1" Ref="RV15"  Part="1" 
AR Path="/6143F4F0/6143DDE1" Ref="RV?"  Part="1" 
F 0 "RV15" H 3080 4846 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 4755 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 4800 50  0001 C CNN
F 3 "~" H 3150 4800 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DDE2
P 3150 5150
AR Path="/61044A2C/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDE2" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDE2" Ref="#PWR0155"  Part="1" 
AR Path="/6143F4F0/6143DDE2" Ref="#PWR?"  Part="1" 
F 0 "#PWR0155" H 3150 4900 50  0001 C CNN
F 1 "GND" H 3155 4977 50  0000 C CNN
F 2 "" H 3150 5150 50  0001 C CNN
F 3 "" H 3150 5150 50  0001 C CNN
	1    3150 5150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 6108D936
P 3150 4500
AR Path="/61044A2C/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6108D936" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6108D936" Ref="#PWR0152"  Part="1" 
AR Path="/6143F4F0/6108D936" Ref="#PWR?"  Part="1" 
F 0 "#PWR0152" H 3150 4350 50  0001 C CNN
F 1 "+12V" H 3165 4673 50  0000 C CNN
F 2 "" H 3150 4500 50  0001 C CNN
F 3 "" H 3150 4500 50  0001 C CNN
	1    3150 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4650 3150 4500
Wire Wire Line
	3150 5150 3150 4950
Wire Wire Line
	3400 4100 3510 4100
Connection ~ 5200 3400
Wire Wire Line
	5200 3850 5200 4250
Connection ~ 5200 4700
Wire Wire Line
	5200 5150 5200 5500
$Comp
L Device:C C?
U 1 1 614602DC
P 4395 3750
AR Path="/61044A2C/614602DC" Ref="C?"  Part="1" 
AR Path="/61086D02/614602DC" Ref="C?"  Part="1" 
AR Path="/61422789/614602DC" Ref="C?"  Part="1" 
AR Path="/61422AED/614602DC" Ref="C?"  Part="1" 
AR Path="/61424501/614602DC" Ref="C?"  Part="1" 
AR Path="/6142478E/614602DC" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614602DC" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614602DC" Ref="C?"  Part="1" 
AR Path="/61439586/614602DC" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614602DC" Ref="C41"  Part="1" 
AR Path="/6143F4F0/614602DC" Ref="C?"  Part="1" 
F 0 "C41" H 4510 3796 50  0000 L CNN
F 1 "0.1uF" H 4510 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4433 3600 50  0001 C CNN
F 3 "~" H 4395 3750 50  0001 C CNN
	1    4395 3750
	1    0    0    -1  
$EndComp
Connection ~ 3510 4100
$Comp
L power:GND #PWR?
U 1 1 614602EE
P 3510 4500
AR Path="/61044A2C/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602EE" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602EE" Ref="#PWR0153"  Part="1" 
AR Path="/6143F4F0/614602EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR0153" H 3510 4250 50  0001 C CNN
F 1 "GND" H 3515 4327 50  0000 C CNN
F 2 "" H 3510 4500 50  0001 C CNN
F 3 "" H 3510 4500 50  0001 C CNN
	1    3510 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3510 4030 3510 4100
Wire Wire Line
	4200 3500 4200 4100
$Comp
L Device:R R?
U 1 1 614602E9
P 3870 4100
AR Path="/61044A2C/614602E9" Ref="R?"  Part="1" 
AR Path="/61086D02/614602E9" Ref="R?"  Part="1" 
AR Path="/61422789/614602E9" Ref="R?"  Part="1" 
AR Path="/61422AED/614602E9" Ref="R?"  Part="1" 
AR Path="/61424501/614602E9" Ref="R?"  Part="1" 
AR Path="/6142478E/614602E9" Ref="R?"  Part="1" 
AR Path="/6142B4BA/614602E9" Ref="R?"  Part="1" 
AR Path="/6142D6FB/614602E9" Ref="R?"  Part="1" 
AR Path="/61439586/614602E9" Ref="R?"  Part="1" 
AR Path="/6143C8B2/614602E9" Ref="R35"  Part="1" 
AR Path="/6143F4F0/614602E9" Ref="R?"  Part="1" 
F 0 "R35" V 3970 4100 50  0000 C CNN
F 1 "50k" V 3870 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3800 4100 50  0001 C CNN
F 3 "~" H 3870 4100 50  0001 C CNN
	1    3870 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 6143DDEE
P 4080 4290
AR Path="/61044A2C/6143DDEE" Ref="R?"  Part="1" 
AR Path="/61086D02/6143DDEE" Ref="R?"  Part="1" 
AR Path="/61422789/6143DDEE" Ref="R?"  Part="1" 
AR Path="/61422AED/6143DDEE" Ref="R?"  Part="1" 
AR Path="/61424501/6143DDEE" Ref="R?"  Part="1" 
AR Path="/6142478E/6143DDEE" Ref="R?"  Part="1" 
AR Path="/6142B4BA/6143DDEE" Ref="R?"  Part="1" 
AR Path="/6142D6FB/6143DDEE" Ref="R?"  Part="1" 
AR Path="/61439586/6143DDEE" Ref="R?"  Part="1" 
AR Path="/6143C8B2/6143DDEE" Ref="R36"  Part="1" 
AR Path="/6143F4F0/6143DDEE" Ref="R?"  Part="1" 
F 0 "R36" V 4180 4290 50  0000 C CNN
F 1 "50k" V 4080 4290 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4010 4290 50  0001 C CNN
F 3 "~" H 4080 4290 50  0001 C CNN
	1    4080 4290
	-1   0    0    1   
$EndComp
Wire Wire Line
	3510 4500 3510 4470
Wire Wire Line
	3510 4100 3720 4100
Wire Wire Line
	4020 4100 4080 4100
Connection ~ 4200 4100
Wire Wire Line
	4200 4100 4200 4600
Wire Wire Line
	4080 4140 4080 4100
Connection ~ 4080 4100
Wire Wire Line
	4080 4100 4200 4100
Wire Wire Line
	4080 4440 4080 4470
Wire Wire Line
	4080 4470 3510 4470
Wire Wire Line
	4395 3970 4395 3900
Wire Wire Line
	5200 3400 5640 3400
Wire Wire Line
	5200 4700 5640 4700
Wire Wire Line
	3300 3300 3670 3300
Wire Wire Line
	3300 4800 3630 4800
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 6143DDEF
P 8010 3980
AR Path="/61044A2C/6143DDEF" Ref="J?"  Part="1" 
AR Path="/61422789/6143DDEF" Ref="J?"  Part="1" 
AR Path="/61422AED/6143DDEF" Ref="J?"  Part="1" 
AR Path="/61424501/6143DDEF" Ref="J?"  Part="1" 
AR Path="/6142478E/6143DDEF" Ref="J?"  Part="1" 
AR Path="/6142B4BA/6143DDEF" Ref="J?"  Part="1" 
AR Path="/6142D6FB/6143DDEF" Ref="J?"  Part="1" 
AR Path="/61439586/6143DDEF" Ref="J?"  Part="1" 
AR Path="/6143C8B2/6143DDEF" Ref="J16"  Part="1" 
AR Path="/6143F4F0/6143DDEF" Ref="J?"  Part="1" 
F 0 "J16" H 8060 4197 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 8060 4106 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 8010 3980 50  0001 C CNN
F 3 "~" H 8010 3980 50  0001 C CNN
	1    8010 3980
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6142825D
P 8600 4140
AR Path="/61044A2C/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6142825D" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6142825D" Ref="#PWR0150"  Part="1" 
AR Path="/6143F4F0/6142825D" Ref="#PWR?"  Part="1" 
F 0 "#PWR0150" H 8600 3890 50  0001 C CNN
F 1 "GND" H 8605 3967 50  0000 C CNN
F 2 "" H 8600 4140 50  0001 C CNN
F 3 "" H 8600 4140 50  0001 C CNN
	1    8600 4140
	1    0    0    -1  
$EndComp
Wire Wire Line
	8310 3980 8600 3980
Wire Wire Line
	8600 3980 8600 4080
Wire Wire Line
	8310 4080 8600 4080
Connection ~ 8600 4080
Wire Wire Line
	8600 4080 8600 4140
Text Label 3670 3130 0    50   ~ 0
HumLowSet
Text Label 3680 4930 0    50   ~ 0
HumHighSet
Wire Wire Line
	3670 3130 3670 3300
Connection ~ 3670 3300
Wire Wire Line
	3670 3300 4500 3300
Wire Wire Line
	3680 4930 3630 4930
Wire Wire Line
	3630 4930 3630 4800
Connection ~ 3630 4800
Wire Wire Line
	3630 4800 4500 4800
Text Label 7690 3980 2    50   ~ 0
HumLowSet
Text Label 7690 4080 2    50   ~ 0
HumHighSet
Wire Wire Line
	7690 3980 7810 3980
Wire Wire Line
	7810 4080 7690 4080
Text Notes 1470 5590 0    50   ~ 0
Generic input (HumIn):\n- for resistor type: load R35 with 0 Ohm, do not load R36; R34 and NTCIn act as voltage divider\n- for voltage type: do not load R34; R35 and R36 act as voltage divider
Text Notes 1060 4260 0    50   ~ 0
Use HIH-4021-003 for humidity sensor
Text HLabel 5800 3500 0    50   Output ~ 0
HumLowOut
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 61685F2B
P 6150 3500
AR Path="/61119591/61685F2B" Ref="S?"  Part="1" 
AR Path="/614C2E34/61685F2B" Ref="S?"  Part="1" 
AR Path="/614C4D35/61685F2B" Ref="S?"  Part="1" 
AR Path="/6143C8B2/61685F2B" Ref="S17"  Part="1" 
F 0 "S17" H 6270 3546 50  0000 L CNN
F 1 "SW" H 6270 3455 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 3500 50  0001 L BNN
F 3 "" H 6150 3500 50  0001 L BNN
F 4 "C&K" H 6150 3500 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 3500 50  0001 L BNN "STANDARD"
	1    6150 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61685F31
P 5850 3600
AR Path="/61119591/61685F31" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/61685F31" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/61685F31" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61685F31" Ref="#PWR0147"  Part="1" 
F 0 "#PWR0147" H 5850 3350 50  0001 C CNN
F 1 "GND" H 5855 3427 50  0000 C CNN
F 2 "" H 5850 3600 50  0001 C CNN
F 3 "" H 5850 3600 50  0001 C CNN
	1    5850 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3600 5850 3600
Wire Wire Line
	5950 3500 5800 3500
Text HLabel 5800 4800 0    50   Output ~ 0
HumHighOut
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 61687C9E
P 6150 4800
AR Path="/61119591/61687C9E" Ref="S?"  Part="1" 
AR Path="/614C2E34/61687C9E" Ref="S?"  Part="1" 
AR Path="/614C4D35/61687C9E" Ref="S?"  Part="1" 
AR Path="/6143C8B2/61687C9E" Ref="S18"  Part="1" 
F 0 "S18" H 6270 4846 50  0000 L CNN
F 1 "SW" H 6270 4755 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 4800 50  0001 L BNN
F 3 "" H 6150 4800 50  0001 L BNN
F 4 "C&K" H 6150 4800 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 4800 50  0001 L BNN "STANDARD"
	1    6150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61687CA4
P 5850 4900
AR Path="/61119591/61687CA4" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/61687CA4" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/61687CA4" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61687CA4" Ref="#PWR0154"  Part="1" 
F 0 "#PWR0154" H 5850 4650 50  0001 C CNN
F 1 "GND" H 5855 4727 50  0000 C CNN
F 2 "" H 5850 4900 50  0001 C CNN
F 3 "" H 5850 4900 50  0001 C CNN
	1    5850 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4900 5850 4900
Wire Wire Line
	5950 4800 5800 4800
$Comp
L Device:R R?
U 1 1 6149F958
P 5640 3140
AR Path="/61044A2C/6149F958" Ref="R?"  Part="1" 
AR Path="/61086D02/6149F958" Ref="R?"  Part="1" 
AR Path="/61422789/6149F958" Ref="R?"  Part="1" 
AR Path="/61422AED/6149F958" Ref="R?"  Part="1" 
AR Path="/61424501/6149F958" Ref="R?"  Part="1" 
AR Path="/6142478E/6149F958" Ref="R?"  Part="1" 
AR Path="/614278B4/6149F958" Ref="R?"  Part="1" 
AR Path="/6143C8B2/6149F958" Ref="R77"  Part="1" 
F 0 "R77" V 5740 3140 50  0000 C CNN
F 1 "50k" V 5640 3140 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5570 3140 50  0001 C CNN
F 3 "~" H 5640 3140 50  0001 C CNN
	1    5640 3140
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 614A0B7C
P 5640 4470
AR Path="/61044A2C/614A0B7C" Ref="R?"  Part="1" 
AR Path="/61086D02/614A0B7C" Ref="R?"  Part="1" 
AR Path="/61422789/614A0B7C" Ref="R?"  Part="1" 
AR Path="/61422AED/614A0B7C" Ref="R?"  Part="1" 
AR Path="/61424501/614A0B7C" Ref="R?"  Part="1" 
AR Path="/6142478E/614A0B7C" Ref="R?"  Part="1" 
AR Path="/614278B4/614A0B7C" Ref="R?"  Part="1" 
AR Path="/6143C8B2/614A0B7C" Ref="R78"  Part="1" 
F 0 "R78" V 5740 4470 50  0000 C CNN
F 1 "50k" V 5640 4470 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5570 4470 50  0001 C CNN
F 3 "~" H 5640 4470 50  0001 C CNN
	1    5640 4470
	-1   0    0    1   
$EndComp
Wire Wire Line
	5640 3290 5640 3400
Connection ~ 5640 3400
Wire Wire Line
	5640 3400 5950 3400
Wire Wire Line
	5640 4620 5640 4700
Connection ~ 5640 4700
Wire Wire Line
	5640 4700 5950 4700
$Comp
L power:GND #PWR?
U 1 1 615CEF52
P 4395 3970
AR Path="/61044A2C/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/61422789/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/61424501/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/61439586/615CEF52" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/615CEF52" Ref="#PWR0246"  Part="1" 
AR Path="/6143F4F0/615CEF52" Ref="#PWR?"  Part="1" 
F 0 "#PWR0246" H 4395 3720 50  0001 C CNN
F 1 "GND" H 4400 3797 50  0000 C CNN
F 2 "" H 4395 3970 50  0001 C CNN
F 3 "" H 4395 3970 50  0001 C CNN
	1    4395 3970
	1    0    0    -1  
$EndComp
Wire Wire Line
	4395 3600 4395 3500
Connection ~ 4395 3500
Wire Wire Line
	4395 3500 4500 3500
$Comp
L power:+12V #PWR?
U 1 1 618AEED3
P 5640 2690
AR Path="/61044A2C/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/614278B4/618AEED3" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/618AEED3" Ref="#PWR0233"  Part="1" 
F 0 "#PWR0233" H 5640 2540 50  0001 C CNN
F 1 "+12V" H 5655 2863 50  0000 C CNN
F 2 "" H 5640 2690 50  0001 C CNN
F 3 "" H 5640 2690 50  0001 C CNN
	1    5640 2690
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618AEED9
P 5640 2840
AR Path="/61119591/618AEED9" Ref="D?"  Part="1" 
AR Path="/614C2E34/618AEED9" Ref="D?"  Part="1" 
AR Path="/614C4D35/618AEED9" Ref="D?"  Part="1" 
AR Path="/61422AED/618AEED9" Ref="D?"  Part="1" 
AR Path="/6143C8B2/618AEED9" Ref="D38"  Part="1" 
F 0 "D38" V 5679 2722 50  0000 R CNN
F 1 "LED" V 5588 2722 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5640 2840 50  0001 C CNN
F 3 "~" H 5640 2840 50  0001 C CNN
	1    5640 2840
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 618B2086
P 5640 4020
AR Path="/61044A2C/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/614278B4/618B2086" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/618B2086" Ref="#PWR0234"  Part="1" 
F 0 "#PWR0234" H 5640 3870 50  0001 C CNN
F 1 "+12V" H 5655 4193 50  0000 C CNN
F 2 "" H 5640 4020 50  0001 C CNN
F 3 "" H 5640 4020 50  0001 C CNN
	1    5640 4020
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618B208C
P 5640 4170
AR Path="/61119591/618B208C" Ref="D?"  Part="1" 
AR Path="/614C2E34/618B208C" Ref="D?"  Part="1" 
AR Path="/614C4D35/618B208C" Ref="D?"  Part="1" 
AR Path="/61422AED/618B208C" Ref="D?"  Part="1" 
AR Path="/6143C8B2/618B208C" Ref="D39"  Part="1" 
F 0 "D39" V 5679 4052 50  0000 R CNN
F 1 "LED" V 5588 4052 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5640 4170 50  0001 C CNN
F 3 "~" H 5640 4170 50  0001 C CNN
	1    5640 4170
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
