EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM393 U?
U 1 1 614602D6
P 4800 3400
AR Path="/61044A2C/614602D6" Ref="U?"  Part="1" 
AR Path="/61086D02/614602D6" Ref="U?"  Part="1" 
AR Path="/61422789/614602D6" Ref="U?"  Part="1" 
AR Path="/61422AED/614602D6" Ref="U?"  Part="1" 
AR Path="/61424501/614602D6" Ref="U?"  Part="1" 
AR Path="/6142478E/614602D6" Ref="U?"  Part="1" 
AR Path="/6142B4BA/614602D6" Ref="U?"  Part="1" 
AR Path="/6142D6FB/614602D6" Ref="U?"  Part="1" 
AR Path="/61439586/614602D6" Ref="U?"  Part="1" 
AR Path="/6143C8B2/614602D6" Ref="U?"  Part="1" 
AR Path="/6143F4F0/614602D6" Ref="U13"  Part="1" 
F 0 "U13" H 4800 3767 50  0000 C CNN
F 1 "LM393" H 4800 3676 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 2 1 614602DE
P 4800 4700
AR Path="/61044A2C/614602DE" Ref="U?"  Part="2" 
AR Path="/61086D02/614602DE" Ref="U?"  Part="2" 
AR Path="/61422789/614602DE" Ref="U?"  Part="2" 
AR Path="/61422AED/614602DE" Ref="U?"  Part="2" 
AR Path="/61424501/614602DE" Ref="U?"  Part="2" 
AR Path="/6142478E/614602DE" Ref="U?"  Part="2" 
AR Path="/6142B4BA/614602DE" Ref="U?"  Part="2" 
AR Path="/6142D6FB/614602DE" Ref="U?"  Part="2" 
AR Path="/61439586/614602DE" Ref="U?"  Part="2" 
AR Path="/6143C8B2/614602DE" Ref="U?"  Part="2" 
AR Path="/6143F4F0/614602DE" Ref="U13"  Part="2" 
F 0 "U13" H 4800 5067 50  0000 C CNN
F 1 "LM393" H 4800 4976 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 4700 50  0001 C CNN
	2    4800 4700
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 3 1 614602DF
P 4650 1950
AR Path="/61044A2C/614602DF" Ref="U?"  Part="3" 
AR Path="/61086D02/614602DF" Ref="U?"  Part="3" 
AR Path="/61422789/614602DF" Ref="U?"  Part="3" 
AR Path="/61422AED/614602DF" Ref="U?"  Part="3" 
AR Path="/61424501/614602DF" Ref="U?"  Part="3" 
AR Path="/6142478E/614602DF" Ref="U?"  Part="3" 
AR Path="/6142B4BA/614602DF" Ref="U?"  Part="3" 
AR Path="/6142D6FB/614602DF" Ref="U?"  Part="3" 
AR Path="/61439586/614602DF" Ref="U?"  Part="3" 
AR Path="/6143C8B2/614602DF" Ref="U?"  Part="3" 
AR Path="/6143F4F0/614602DF" Ref="U13"  Part="3" 
F 0 "U13" H 4608 1996 50  0000 L CNN
F 1 "LM393" H 4608 1905 50  0000 L CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4650 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4650 1950 50  0001 C CNN
	3    4650 1950
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 6143DDDE
P 5150 1500
AR Path="/61044A2C/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDDE" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6143DDDE" Ref="#PWR0157"  Part="1" 
F 0 "#PWR0157" H 5150 1350 50  0001 C CNN
F 1 "+12V" H 5165 1673 50  0000 C CNN
F 2 "" H 5150 1500 50  0001 C CNN
F 3 "" H 5150 1500 50  0001 C CNN
	1    5150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1850 4950 1850
$Comp
L power:GND #PWR?
U 1 1 6143DDDF
P 4100 1900
AR Path="/61044A2C/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDDF" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6143DDDF" Ref="#PWR0159"  Part="1" 
F 0 "#PWR0159" H 4100 1650 50  0001 C CNN
F 1 "GND" H 4105 1727 50  0000 C CNN
F 2 "" H 4100 1900 50  0001 C CNN
F 3 "" H 4100 1900 50  0001 C CNN
	1    4100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1850 4100 1850
Wire Wire Line
	4100 1850 4100 1900
$Comp
L Device:C C?
U 1 1 6143DDEC
P 5450 1850
AR Path="/61044A2C/6143DDEC" Ref="C?"  Part="1" 
AR Path="/61086D02/6143DDEC" Ref="C?"  Part="1" 
AR Path="/61422789/6143DDEC" Ref="C?"  Part="1" 
AR Path="/61422AED/6143DDEC" Ref="C?"  Part="1" 
AR Path="/61424501/6143DDEC" Ref="C?"  Part="1" 
AR Path="/6142478E/6143DDEC" Ref="C?"  Part="1" 
AR Path="/6142B4BA/6143DDEC" Ref="C?"  Part="1" 
AR Path="/6142D6FB/6143DDEC" Ref="C?"  Part="1" 
AR Path="/61439586/6143DDEC" Ref="C?"  Part="1" 
AR Path="/6143C8B2/6143DDEC" Ref="C?"  Part="1" 
AR Path="/6143F4F0/6143DDEC" Ref="C43"  Part="1" 
F 0 "C43" V 5600 1850 50  0000 C CNN
F 1 "0.1uF" V 5289 1850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5488 1700 50  0001 C CNN
F 3 "~" H 5450 1850 50  0001 C CNN
	1    5450 1850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DDE0
P 5800 1850
AR Path="/61044A2C/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDE0" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6143DDE0" Ref="#PWR0158"  Part="1" 
F 0 "#PWR0158" H 5800 1600 50  0001 C CNN
F 1 "GND" H 5805 1677 50  0000 C CNN
F 2 "" H 5800 1850 50  0001 C CNN
F 3 "" H 5800 1850 50  0001 C CNN
	1    5800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1850 5600 1850
Wire Wire Line
	5300 1850 5150 1850
Connection ~ 5150 1850
$Comp
L power:+12V #PWR?
U 1 1 614602E7
P 2500 3850
AR Path="/61044A2C/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602E7" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/614602E7" Ref="#PWR0163"  Part="1" 
F 0 "#PWR0163" H 2500 3700 50  0001 C CNN
F 1 "+12V" H 2515 4023 50  0000 C CNN
F 2 "" H 2500 3850 50  0001 C CNN
F 3 "" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 614245C9
P 3250 4100
AR Path="/61044A2C/614245C9" Ref="R?"  Part="1" 
AR Path="/61086D02/614245C9" Ref="R?"  Part="1" 
AR Path="/61422789/614245C9" Ref="R?"  Part="1" 
AR Path="/61422AED/614245C9" Ref="R?"  Part="1" 
AR Path="/61424501/614245C9" Ref="R?"  Part="1" 
AR Path="/6142478E/614245C9" Ref="R?"  Part="1" 
AR Path="/6142B4BA/614245C9" Ref="R?"  Part="1" 
AR Path="/6142D6FB/614245C9" Ref="R?"  Part="1" 
AR Path="/61439586/614245C9" Ref="R?"  Part="1" 
AR Path="/6143C8B2/614245C9" Ref="R?"  Part="1" 
AR Path="/6143F4F0/614245C9" Ref="R37"  Part="1" 
F 0 "R37" V 3350 4100 50  0000 C CNN
F 1 "50k" V 3250 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 4100 50  0001 C CNN
F 3 "~" H 3250 4100 50  0001 C CNN
	1    3250 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 3850 2500 4100
Wire Wire Line
	2500 4100 3100 4100
Wire Wire Line
	4200 3500 4370 3500
Wire Wire Line
	4500 4600 4200 4600
Wire Wire Line
	5150 1500 5150 1850
Wire Wire Line
	5100 3400 5200 3400
$Comp
L Device:C C?
U 1 1 614602E6
P 5200 3700
AR Path="/61044A2C/614602E6" Ref="C?"  Part="1" 
AR Path="/61086D02/614602E6" Ref="C?"  Part="1" 
AR Path="/61422789/614602E6" Ref="C?"  Part="1" 
AR Path="/61422AED/614602E6" Ref="C?"  Part="1" 
AR Path="/61424501/614602E6" Ref="C?"  Part="1" 
AR Path="/6142478E/614602E6" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614602E6" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614602E6" Ref="C?"  Part="1" 
AR Path="/61439586/614602E6" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614602E6" Ref="C?"  Part="1" 
AR Path="/6143F4F0/614602E6" Ref="C44"  Part="1" 
F 0 "C44" H 5315 3746 50  0000 L CNN
F 1 "15pF" H 5315 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 3550 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
	1    5200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DDF2
P 5200 4250
AR Path="/61044A2C/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDF2" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6143DDF2" Ref="#PWR0165"  Part="1" 
F 0 "#PWR0165" H 5200 4000 50  0001 C CNN
F 1 "GND" H 5205 4077 50  0000 C CNN
F 2 "" H 5200 4250 50  0001 C CNN
F 3 "" H 5200 4250 50  0001 C CNN
	1    5200 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 5200 3400
Wire Wire Line
	5100 4700 5200 4700
$Comp
L Device:C C?
U 1 1 614602D9
P 5200 5000
AR Path="/61044A2C/614602D9" Ref="C?"  Part="1" 
AR Path="/61086D02/614602D9" Ref="C?"  Part="1" 
AR Path="/61422789/614602D9" Ref="C?"  Part="1" 
AR Path="/61422AED/614602D9" Ref="C?"  Part="1" 
AR Path="/61424501/614602D9" Ref="C?"  Part="1" 
AR Path="/6142478E/614602D9" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614602D9" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614602D9" Ref="C?"  Part="1" 
AR Path="/61439586/614602D9" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614602D9" Ref="C?"  Part="1" 
AR Path="/6143F4F0/614602D9" Ref="C46"  Part="1" 
F 0 "C46" H 5315 5046 50  0000 L CNN
F 1 "15pF" H 5315 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 4850 50  0001 C CNN
F 3 "~" H 5200 5000 50  0001 C CNN
	1    5200 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DDF1
P 5200 5500
AR Path="/61044A2C/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDF1" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6143DDF1" Ref="#PWR0170"  Part="1" 
F 0 "#PWR0170" H 5200 5250 50  0001 C CNN
F 1 "GND" H 5205 5327 50  0000 C CNN
F 2 "" H 5200 5500 50  0001 C CNN
F 3 "" H 5200 5500 50  0001 C CNN
	1    5200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4850 5200 4700
Text HLabel 3510 4030 1    50   Input ~ 0
FlowIn
$Comp
L Device:R_Potentiometer RV?
U 1 1 614602E5
P 3150 3300
AR Path="/61044A2C/614602E5" Ref="RV?"  Part="1" 
AR Path="/61086D02/614602E5" Ref="RV?"  Part="1" 
AR Path="/61422789/614602E5" Ref="RV?"  Part="1" 
AR Path="/61422AED/614602E5" Ref="RV?"  Part="1" 
AR Path="/61424501/614602E5" Ref="RV?"  Part="1" 
AR Path="/6142478E/614602E5" Ref="RV?"  Part="1" 
AR Path="/6142B4BA/614602E5" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/614602E5" Ref="RV?"  Part="1" 
AR Path="/61439586/614602E5" Ref="RV?"  Part="1" 
AR Path="/6143C8B2/614602E5" Ref="RV?"  Part="1" 
AR Path="/6143F4F0/614602E5" Ref="RV16"  Part="1" 
F 0 "RV16" H 3080 3346 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 3255 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 3300 50  0001 C CNN
F 3 "~" H 3150 3300 50  0001 C CNN
	1    3150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614245D8
P 3150 3650
AR Path="/61044A2C/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614245D8" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/614245D8" Ref="#PWR0162"  Part="1" 
F 0 "#PWR0162" H 3150 3400 50  0001 C CNN
F 1 "GND" H 3155 3477 50  0000 C CNN
F 2 "" H 3150 3650 50  0001 C CNN
F 3 "" H 3150 3650 50  0001 C CNN
	1    3150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 614602DB
P 3150 3000
AR Path="/61044A2C/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602DB" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/614602DB" Ref="#PWR0160"  Part="1" 
F 0 "#PWR0160" H 3150 2850 50  0001 C CNN
F 1 "+12V" H 3165 3173 50  0000 C CNN
F 2 "" H 3150 3000 50  0001 C CNN
F 3 "" H 3150 3000 50  0001 C CNN
	1    3150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3150 3150 3000
Wire Wire Line
	3150 3650 3150 3450
$Comp
L Device:R_Potentiometer RV?
U 1 1 614602E3
P 3150 4800
AR Path="/61044A2C/614602E3" Ref="RV?"  Part="1" 
AR Path="/61086D02/614602E3" Ref="RV?"  Part="1" 
AR Path="/61422789/614602E3" Ref="RV?"  Part="1" 
AR Path="/61422AED/614602E3" Ref="RV?"  Part="1" 
AR Path="/61424501/614602E3" Ref="RV?"  Part="1" 
AR Path="/6142478E/614602E3" Ref="RV?"  Part="1" 
AR Path="/6142B4BA/614602E3" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/614602E3" Ref="RV?"  Part="1" 
AR Path="/61439586/614602E3" Ref="RV?"  Part="1" 
AR Path="/6143C8B2/614602E3" Ref="RV?"  Part="1" 
AR Path="/6143F4F0/614602E3" Ref="RV17"  Part="1" 
F 0 "RV17" H 3080 4846 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 4755 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 4800 50  0001 C CNN
F 3 "~" H 3150 4800 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614602E4
P 3150 5150
AR Path="/61044A2C/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602E4" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/614602E4" Ref="#PWR0169"  Part="1" 
F 0 "#PWR0169" H 3150 4900 50  0001 C CNN
F 1 "GND" H 3155 4977 50  0000 C CNN
F 2 "" H 3150 5150 50  0001 C CNN
F 3 "" H 3150 5150 50  0001 C CNN
	1    3150 5150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 614602D7
P 3150 4500
AR Path="/61044A2C/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602D7" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/614602D7" Ref="#PWR0166"  Part="1" 
F 0 "#PWR0166" H 3150 4350 50  0001 C CNN
F 1 "+12V" H 3165 4673 50  0000 C CNN
F 2 "" H 3150 4500 50  0001 C CNN
F 3 "" H 3150 4500 50  0001 C CNN
	1    3150 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4650 3150 4500
Wire Wire Line
	3150 5150 3150 4950
Wire Wire Line
	3400 4100 3510 4100
Connection ~ 5200 3400
Wire Wire Line
	5200 3850 5200 4250
Connection ~ 5200 4700
Wire Wire Line
	5200 5150 5200 5500
$Comp
L Device:C C?
U 1 1 614245DF
P 4370 3780
AR Path="/61044A2C/614245DF" Ref="C?"  Part="1" 
AR Path="/61086D02/614245DF" Ref="C?"  Part="1" 
AR Path="/61422789/614245DF" Ref="C?"  Part="1" 
AR Path="/61422AED/614245DF" Ref="C?"  Part="1" 
AR Path="/61424501/614245DF" Ref="C?"  Part="1" 
AR Path="/6142478E/614245DF" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614245DF" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614245DF" Ref="C?"  Part="1" 
AR Path="/61439586/614245DF" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614245DF" Ref="C?"  Part="1" 
AR Path="/6143F4F0/614245DF" Ref="C45"  Part="1" 
F 0 "C45" H 4485 3826 50  0000 L CNN
F 1 "0.1uF" H 4485 3735 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4408 3630 50  0001 C CNN
F 3 "~" H 4370 3780 50  0001 C CNN
	1    4370 3780
	1    0    0    -1  
$EndComp
Connection ~ 3510 4100
$Comp
L power:GND #PWR?
U 1 1 6143DDF3
P 3510 4500
AR Path="/61044A2C/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/61439586/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDF3" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6143DDF3" Ref="#PWR0167"  Part="1" 
F 0 "#PWR0167" H 3510 4250 50  0001 C CNN
F 1 "GND" H 3515 4327 50  0000 C CNN
F 2 "" H 3510 4500 50  0001 C CNN
F 3 "" H 3510 4500 50  0001 C CNN
	1    3510 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3510 4030 3510 4100
Wire Wire Line
	4200 3500 4200 4100
$Comp
L Device:R R?
U 1 1 6143DDED
P 3870 4100
AR Path="/61044A2C/6143DDED" Ref="R?"  Part="1" 
AR Path="/61086D02/6143DDED" Ref="R?"  Part="1" 
AR Path="/61422789/6143DDED" Ref="R?"  Part="1" 
AR Path="/61422AED/6143DDED" Ref="R?"  Part="1" 
AR Path="/61424501/6143DDED" Ref="R?"  Part="1" 
AR Path="/6142478E/6143DDED" Ref="R?"  Part="1" 
AR Path="/6142B4BA/6143DDED" Ref="R?"  Part="1" 
AR Path="/6142D6FB/6143DDED" Ref="R?"  Part="1" 
AR Path="/61439586/6143DDED" Ref="R?"  Part="1" 
AR Path="/6143C8B2/6143DDED" Ref="R?"  Part="1" 
AR Path="/6143F4F0/6143DDED" Ref="R38"  Part="1" 
F 0 "R38" V 3970 4100 50  0000 C CNN
F 1 "50k" V 3870 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3800 4100 50  0001 C CNN
F 3 "~" H 3870 4100 50  0001 C CNN
	1    3870 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 614602EA
P 4080 4290
AR Path="/61044A2C/614602EA" Ref="R?"  Part="1" 
AR Path="/61086D02/614602EA" Ref="R?"  Part="1" 
AR Path="/61422789/614602EA" Ref="R?"  Part="1" 
AR Path="/61422AED/614602EA" Ref="R?"  Part="1" 
AR Path="/61424501/614602EA" Ref="R?"  Part="1" 
AR Path="/6142478E/614602EA" Ref="R?"  Part="1" 
AR Path="/6142B4BA/614602EA" Ref="R?"  Part="1" 
AR Path="/6142D6FB/614602EA" Ref="R?"  Part="1" 
AR Path="/61439586/614602EA" Ref="R?"  Part="1" 
AR Path="/6143C8B2/614602EA" Ref="R?"  Part="1" 
AR Path="/6143F4F0/614602EA" Ref="R39"  Part="1" 
F 0 "R39" V 4180 4290 50  0000 C CNN
F 1 "50k" V 4080 4290 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4010 4290 50  0001 C CNN
F 3 "~" H 4080 4290 50  0001 C CNN
	1    4080 4290
	-1   0    0    1   
$EndComp
Wire Wire Line
	3510 4500 3510 4470
Wire Wire Line
	3510 4100 3720 4100
Wire Wire Line
	4020 4100 4080 4100
Connection ~ 4200 4100
Wire Wire Line
	4200 4100 4200 4600
Wire Wire Line
	4080 4140 4080 4100
Connection ~ 4080 4100
Wire Wire Line
	4080 4100 4200 4100
Wire Wire Line
	4080 4440 4080 4470
Wire Wire Line
	4080 4470 3510 4470
Wire Wire Line
	4370 4000 4370 3930
Wire Wire Line
	5200 3400 5580 3400
Wire Wire Line
	5200 4700 5610 4700
Wire Wire Line
	3300 3300 3670 3300
Wire Wire Line
	3300 4800 3630 4800
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 614602EB
P 8010 3980
AR Path="/61044A2C/614602EB" Ref="J?"  Part="1" 
AR Path="/61422789/614602EB" Ref="J?"  Part="1" 
AR Path="/61422AED/614602EB" Ref="J?"  Part="1" 
AR Path="/61424501/614602EB" Ref="J?"  Part="1" 
AR Path="/6142478E/614602EB" Ref="J?"  Part="1" 
AR Path="/6142B4BA/614602EB" Ref="J?"  Part="1" 
AR Path="/6142D6FB/614602EB" Ref="J?"  Part="1" 
AR Path="/61439586/614602EB" Ref="J?"  Part="1" 
AR Path="/6143C8B2/614602EB" Ref="J?"  Part="1" 
AR Path="/6143F4F0/614602EB" Ref="J17"  Part="1" 
F 0 "J17" H 8060 4197 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 8060 4106 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 8010 3980 50  0001 C CNN
F 3 "~" H 8010 3980 50  0001 C CNN
	1    8010 3980
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 614602DD
P 8600 4140
AR Path="/61044A2C/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/61422789/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/61424501/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/61439586/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/614602DD" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/614602DD" Ref="#PWR0164"  Part="1" 
F 0 "#PWR0164" H 8600 3890 50  0001 C CNN
F 1 "GND" H 8605 3967 50  0000 C CNN
F 2 "" H 8600 4140 50  0001 C CNN
F 3 "" H 8600 4140 50  0001 C CNN
	1    8600 4140
	1    0    0    -1  
$EndComp
Wire Wire Line
	8310 3980 8600 3980
Wire Wire Line
	8600 3980 8600 4080
Wire Wire Line
	8310 4080 8600 4080
Connection ~ 8600 4080
Wire Wire Line
	8600 4080 8600 4140
Text Label 3670 3130 0    50   ~ 0
FlowLowSet
Text Label 3680 4930 0    50   ~ 0
FlowHighSet
Wire Wire Line
	3670 3130 3670 3300
Connection ~ 3670 3300
Wire Wire Line
	3670 3300 4500 3300
Wire Wire Line
	3680 4930 3630 4930
Wire Wire Line
	3630 4930 3630 4800
Connection ~ 3630 4800
Wire Wire Line
	3630 4800 4500 4800
Text Label 7690 3980 2    50   ~ 0
FlowLowSet
Text Label 7690 4080 2    50   ~ 0
FlowHighSet
Wire Wire Line
	7690 3980 7810 3980
Wire Wire Line
	7810 4080 7690 4080
Text Notes 1470 5590 0    50   ~ 0
Generic input (FlowIn):\n- for resistor type: load R38 with 0 Ohm, do not load R39; R37 and NTCIn act as voltage divider\n- for voltage type: do not load R37; R38 and R39 act as voltage divider
Text Notes 1060 4260 0    50   ~ 0
Use SFM3020 for air flow sensor
Text HLabel 5800 3500 0    50   Output ~ 0
FlowLowOut
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 6168C5B7
P 6150 3500
AR Path="/61119591/6168C5B7" Ref="S?"  Part="1" 
AR Path="/614C2E34/6168C5B7" Ref="S?"  Part="1" 
AR Path="/614C4D35/6168C5B7" Ref="S?"  Part="1" 
AR Path="/6143F4F0/6168C5B7" Ref="S19"  Part="1" 
F 0 "S19" H 6270 3546 50  0000 L CNN
F 1 "SW" H 6270 3455 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 3500 50  0001 L BNN
F 3 "" H 6150 3500 50  0001 L BNN
F 4 "C&K" H 6150 3500 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 3500 50  0001 L BNN "STANDARD"
	1    6150 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6168C5BD
P 5850 3600
AR Path="/61119591/6168C5BD" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/6168C5BD" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/6168C5BD" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6168C5BD" Ref="#PWR0161"  Part="1" 
F 0 "#PWR0161" H 5850 3350 50  0001 C CNN
F 1 "GND" H 5855 3427 50  0000 C CNN
F 2 "" H 5850 3600 50  0001 C CNN
F 3 "" H 5850 3600 50  0001 C CNN
	1    5850 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3600 5850 3600
Wire Wire Line
	5950 3500 5800 3500
Text HLabel 5800 4800 0    50   Output ~ 0
FlowHighOut
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 6168E640
P 6150 4800
AR Path="/61119591/6168E640" Ref="S?"  Part="1" 
AR Path="/614C2E34/6168E640" Ref="S?"  Part="1" 
AR Path="/614C4D35/6168E640" Ref="S?"  Part="1" 
AR Path="/6143F4F0/6168E640" Ref="S20"  Part="1" 
F 0 "S20" H 6270 4846 50  0000 L CNN
F 1 "SW" H 6270 4755 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 4800 50  0001 L BNN
F 3 "" H 6150 4800 50  0001 L BNN
F 4 "C&K" H 6150 4800 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 4800 50  0001 L BNN "STANDARD"
	1    6150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6168E646
P 5850 4900
AR Path="/61119591/6168E646" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/6168E646" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/6168E646" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/6168E646" Ref="#PWR0168"  Part="1" 
F 0 "#PWR0168" H 5850 4650 50  0001 C CNN
F 1 "GND" H 5855 4727 50  0000 C CNN
F 2 "" H 5850 4900 50  0001 C CNN
F 3 "" H 5850 4900 50  0001 C CNN
	1    5850 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4900 5850 4900
Wire Wire Line
	5950 4800 5800 4800
$Comp
L Device:R R?
U 1 1 614A5F69
P 5580 3120
AR Path="/61044A2C/614A5F69" Ref="R?"  Part="1" 
AR Path="/61086D02/614A5F69" Ref="R?"  Part="1" 
AR Path="/61422789/614A5F69" Ref="R?"  Part="1" 
AR Path="/61422AED/614A5F69" Ref="R?"  Part="1" 
AR Path="/61424501/614A5F69" Ref="R?"  Part="1" 
AR Path="/6142478E/614A5F69" Ref="R?"  Part="1" 
AR Path="/614278B4/614A5F69" Ref="R?"  Part="1" 
AR Path="/6143F4F0/614A5F69" Ref="R79"  Part="1" 
F 0 "R79" V 5680 3120 50  0000 C CNN
F 1 "50k" V 5580 3120 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5510 3120 50  0001 C CNN
F 3 "~" H 5580 3120 50  0001 C CNN
	1    5580 3120
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 614A7052
P 5610 4440
AR Path="/61044A2C/614A7052" Ref="R?"  Part="1" 
AR Path="/61086D02/614A7052" Ref="R?"  Part="1" 
AR Path="/61422789/614A7052" Ref="R?"  Part="1" 
AR Path="/61422AED/614A7052" Ref="R?"  Part="1" 
AR Path="/61424501/614A7052" Ref="R?"  Part="1" 
AR Path="/6142478E/614A7052" Ref="R?"  Part="1" 
AR Path="/614278B4/614A7052" Ref="R?"  Part="1" 
AR Path="/6143F4F0/614A7052" Ref="R80"  Part="1" 
F 0 "R80" V 5710 4440 50  0000 C CNN
F 1 "50k" V 5610 4440 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5540 4440 50  0001 C CNN
F 3 "~" H 5610 4440 50  0001 C CNN
	1    5610 4440
	-1   0    0    1   
$EndComp
Wire Wire Line
	5580 3270 5580 3400
Connection ~ 5580 3400
Wire Wire Line
	5580 3400 5950 3400
Wire Wire Line
	5610 4590 5610 4700
Connection ~ 5610 4700
Wire Wire Line
	5610 4700 5950 4700
Wire Wire Line
	4370 3630 4370 3500
Connection ~ 4370 3500
Wire Wire Line
	4370 3500 4500 3500
$Comp
L power:GND #PWR?
U 1 1 615CA057
P 4370 4000
AR Path="/61044A2C/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/61422789/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/61424501/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/61439586/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/615CA057" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/615CA057" Ref="#PWR0247"  Part="1" 
F 0 "#PWR0247" H 4370 3750 50  0001 C CNN
F 1 "GND" H 4375 3827 50  0000 C CNN
F 2 "" H 4370 4000 50  0001 C CNN
F 3 "" H 4370 4000 50  0001 C CNN
	1    4370 4000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 618B63F9
P 5580 2670
AR Path="/61044A2C/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/614278B4/618B63F9" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/618B63F9" Ref="#PWR0235"  Part="1" 
F 0 "#PWR0235" H 5580 2520 50  0001 C CNN
F 1 "+12V" H 5595 2843 50  0000 C CNN
F 2 "" H 5580 2670 50  0001 C CNN
F 3 "" H 5580 2670 50  0001 C CNN
	1    5580 2670
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618B63FF
P 5580 2820
AR Path="/61119591/618B63FF" Ref="D?"  Part="1" 
AR Path="/614C2E34/618B63FF" Ref="D?"  Part="1" 
AR Path="/614C4D35/618B63FF" Ref="D?"  Part="1" 
AR Path="/61422AED/618B63FF" Ref="D?"  Part="1" 
AR Path="/6143F4F0/618B63FF" Ref="D40"  Part="1" 
F 0 "D40" V 5619 2702 50  0000 R CNN
F 1 "LED" V 5528 2702 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5580 2820 50  0001 C CNN
F 3 "~" H 5580 2820 50  0001 C CNN
	1    5580 2820
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 618B8EE8
P 5610 3990
AR Path="/61044A2C/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/614278B4/618B8EE8" Ref="#PWR?"  Part="1" 
AR Path="/6143F4F0/618B8EE8" Ref="#PWR0236"  Part="1" 
F 0 "#PWR0236" H 5610 3840 50  0001 C CNN
F 1 "+12V" H 5625 4163 50  0000 C CNN
F 2 "" H 5610 3990 50  0001 C CNN
F 3 "" H 5610 3990 50  0001 C CNN
	1    5610 3990
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618B8EEE
P 5610 4140
AR Path="/61119591/618B8EEE" Ref="D?"  Part="1" 
AR Path="/614C2E34/618B8EEE" Ref="D?"  Part="1" 
AR Path="/614C4D35/618B8EEE" Ref="D?"  Part="1" 
AR Path="/61422AED/618B8EEE" Ref="D?"  Part="1" 
AR Path="/6143F4F0/618B8EEE" Ref="D41"  Part="1" 
F 0 "D41" V 5649 4022 50  0000 R CNN
F 1 "LED" V 5558 4022 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5610 4140 50  0001 C CNN
F 3 "~" H 5610 4140 50  0001 C CNN
	1    5610 4140
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
