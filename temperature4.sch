EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM393 U?
U 1 1 6143964D
P 4800 3400
AR Path="/61044A2C/6143964D" Ref="U?"  Part="1" 
AR Path="/61086D02/6143964D" Ref="U?"  Part="1" 
AR Path="/61422789/6143964D" Ref="U?"  Part="1" 
AR Path="/61422AED/6143964D" Ref="U?"  Part="1" 
AR Path="/61424501/6143964D" Ref="U?"  Part="1" 
AR Path="/6142478E/6143964D" Ref="U10"  Part="1" 
AR Path="/6142B4BA/6143964D" Ref="U?"  Part="1" 
AR Path="/6142D6FB/6143964D" Ref="U?"  Part="1" 
F 0 "U10" H 4800 3767 50  0000 C CNN
F 1 "LM393" H 4800 3676 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 2 1 6143964E
P 4800 4700
AR Path="/61044A2C/6143964E" Ref="U?"  Part="2" 
AR Path="/61086D02/6143964E" Ref="U?"  Part="2" 
AR Path="/61422789/6143964E" Ref="U?"  Part="2" 
AR Path="/61422AED/6143964E" Ref="U?"  Part="2" 
AR Path="/61424501/6143964E" Ref="U?"  Part="2" 
AR Path="/6142478E/6143964E" Ref="U10"  Part="2" 
AR Path="/6142B4BA/6143964E" Ref="U?"  Part="2" 
AR Path="/6142D6FB/6143964E" Ref="U?"  Part="2" 
F 0 "U10" H 4800 5067 50  0000 C CNN
F 1 "LM393" H 4800 4976 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 4700 50  0001 C CNN
	2    4800 4700
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 3 1 6108D922
P 4650 1950
AR Path="/61044A2C/6108D922" Ref="U?"  Part="3" 
AR Path="/61086D02/6108D922" Ref="U?"  Part="3" 
AR Path="/61422789/6108D922" Ref="U?"  Part="3" 
AR Path="/61422AED/6108D922" Ref="U?"  Part="3" 
AR Path="/61424501/6108D922" Ref="U?"  Part="3" 
AR Path="/6142478E/6108D922" Ref="U10"  Part="3" 
AR Path="/6142B4BA/6108D922" Ref="U?"  Part="3" 
AR Path="/6142D6FB/6108D922" Ref="U?"  Part="3" 
F 0 "U10" H 4608 1996 50  0000 L CNN
F 1 "LM393" H 4608 1905 50  0000 L CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4650 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4650 1950 50  0001 C CNN
	3    4650 1950
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 61439650
P 5150 1500
AR Path="/61044A2C/61439650" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439650" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439650" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439650" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439650" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439650" Ref="#PWR0115"  Part="1" 
AR Path="/6142B4BA/61439650" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439650" Ref="#PWR?"  Part="1" 
F 0 "#PWR0115" H 5150 1350 50  0001 C CNN
F 1 "+12V" H 5165 1673 50  0000 C CNN
F 2 "" H 5150 1500 50  0001 C CNN
F 3 "" H 5150 1500 50  0001 C CNN
	1    5150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1850 4950 1850
$Comp
L power:GND #PWR?
U 1 1 61439651
P 4100 1900
AR Path="/61044A2C/61439651" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439651" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439651" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439651" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439651" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439651" Ref="#PWR0117"  Part="1" 
AR Path="/6142B4BA/61439651" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439651" Ref="#PWR?"  Part="1" 
F 0 "#PWR0117" H 4100 1650 50  0001 C CNN
F 1 "GND" H 4105 1727 50  0000 C CNN
F 2 "" H 4100 1900 50  0001 C CNN
F 3 "" H 4100 1900 50  0001 C CNN
	1    4100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1850 4100 1850
Wire Wire Line
	4100 1850 4100 1900
$Comp
L Device:C C?
U 1 1 6143965E
P 5450 1850
AR Path="/61044A2C/6143965E" Ref="C?"  Part="1" 
AR Path="/61086D02/6143965E" Ref="C?"  Part="1" 
AR Path="/61422789/6143965E" Ref="C?"  Part="1" 
AR Path="/61422AED/6143965E" Ref="C?"  Part="1" 
AR Path="/61424501/6143965E" Ref="C?"  Part="1" 
AR Path="/6142478E/6143965E" Ref="C31"  Part="1" 
AR Path="/6142B4BA/6143965E" Ref="C?"  Part="1" 
AR Path="/6142D6FB/6143965E" Ref="C?"  Part="1" 
F 0 "C31" V 5600 1850 50  0000 C CNN
F 1 "0.1uF" V 5289 1850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5488 1700 50  0001 C CNN
F 3 "~" H 5450 1850 50  0001 C CNN
	1    5450 1850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61439652
P 5800 1850
AR Path="/61044A2C/61439652" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439652" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439652" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439652" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439652" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439652" Ref="#PWR0116"  Part="1" 
AR Path="/6142B4BA/61439652" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439652" Ref="#PWR?"  Part="1" 
F 0 "#PWR0116" H 5800 1600 50  0001 C CNN
F 1 "GND" H 5805 1677 50  0000 C CNN
F 2 "" H 5800 1850 50  0001 C CNN
F 3 "" H 5800 1850 50  0001 C CNN
	1    5800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1850 5600 1850
Wire Wire Line
	5300 1850 5150 1850
Connection ~ 5150 1850
$Comp
L power:+12V #PWR?
U 1 1 6143965D
P 2500 3850
AR Path="/61044A2C/6143965D" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143965D" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143965D" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143965D" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143965D" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143965D" Ref="#PWR0121"  Part="1" 
AR Path="/6142B4BA/6143965D" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143965D" Ref="#PWR?"  Part="1" 
F 0 "#PWR0121" H 2500 3700 50  0001 C CNN
F 1 "+12V" H 2515 4023 50  0000 C CNN
F 2 "" H 2500 3850 50  0001 C CNN
F 3 "" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 61439657
P 3250 4100
AR Path="/61044A2C/61439657" Ref="R?"  Part="1" 
AR Path="/61086D02/61439657" Ref="R?"  Part="1" 
AR Path="/61422789/61439657" Ref="R?"  Part="1" 
AR Path="/61422AED/61439657" Ref="R?"  Part="1" 
AR Path="/61424501/61439657" Ref="R?"  Part="1" 
AR Path="/6142478E/61439657" Ref="R28"  Part="1" 
AR Path="/6142B4BA/61439657" Ref="R?"  Part="1" 
AR Path="/6142D6FB/61439657" Ref="R?"  Part="1" 
F 0 "R28" V 3350 4100 50  0000 C CNN
F 1 "50k" V 3250 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 4100 50  0001 C CNN
F 3 "~" H 3250 4100 50  0001 C CNN
	1    3250 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 3850 2500 4100
Wire Wire Line
	2500 4100 3100 4100
Wire Wire Line
	4200 3500 4390 3500
Wire Wire Line
	4500 4600 4200 4600
Wire Wire Line
	5150 1500 5150 1850
Wire Wire Line
	5100 3400 5200 3400
$Comp
L Device:C C?
U 1 1 61439658
P 5200 3700
AR Path="/61044A2C/61439658" Ref="C?"  Part="1" 
AR Path="/61086D02/61439658" Ref="C?"  Part="1" 
AR Path="/61422789/61439658" Ref="C?"  Part="1" 
AR Path="/61422AED/61439658" Ref="C?"  Part="1" 
AR Path="/61424501/61439658" Ref="C?"  Part="1" 
AR Path="/6142478E/61439658" Ref="C32"  Part="1" 
AR Path="/6142B4BA/61439658" Ref="C?"  Part="1" 
AR Path="/6142D6FB/61439658" Ref="C?"  Part="1" 
F 0 "C32" H 5315 3746 50  0000 L CNN
F 1 "15pF" H 5315 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 3550 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
	1    5200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61439664
P 5200 4250
AR Path="/61044A2C/61439664" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439664" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439664" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439664" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439664" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439664" Ref="#PWR0123"  Part="1" 
AR Path="/6142B4BA/61439664" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439664" Ref="#PWR?"  Part="1" 
F 0 "#PWR0123" H 5200 4000 50  0001 C CNN
F 1 "GND" H 5205 4077 50  0000 C CNN
F 2 "" H 5200 4250 50  0001 C CNN
F 3 "" H 5200 4250 50  0001 C CNN
	1    5200 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 5200 3400
Wire Wire Line
	5100 4700 5200 4700
$Comp
L Device:C C?
U 1 1 61439659
P 5200 5000
AR Path="/61044A2C/61439659" Ref="C?"  Part="1" 
AR Path="/61086D02/61439659" Ref="C?"  Part="1" 
AR Path="/61422789/61439659" Ref="C?"  Part="1" 
AR Path="/61422AED/61439659" Ref="C?"  Part="1" 
AR Path="/61424501/61439659" Ref="C?"  Part="1" 
AR Path="/6142478E/61439659" Ref="C34"  Part="1" 
AR Path="/6142B4BA/61439659" Ref="C?"  Part="1" 
AR Path="/6142D6FB/61439659" Ref="C?"  Part="1" 
F 0 "C34" H 5315 5046 50  0000 L CNN
F 1 "15pF" H 5315 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 4850 50  0001 C CNN
F 3 "~" H 5200 5000 50  0001 C CNN
	1    5200 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61439663
P 5200 5500
AR Path="/61044A2C/61439663" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439663" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439663" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439663" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439663" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439663" Ref="#PWR0128"  Part="1" 
AR Path="/6142B4BA/61439663" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439663" Ref="#PWR?"  Part="1" 
F 0 "#PWR0128" H 5200 5250 50  0001 C CNN
F 1 "GND" H 5205 5327 50  0000 C CNN
F 2 "" H 5200 5500 50  0001 C CNN
F 3 "" H 5200 5500 50  0001 C CNN
	1    5200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4850 5200 4700
Text Notes 1300 3760 0    50   ~ 0
NTCALUG01A103F: \nB = 3984\nR at 25C: 10k\nR at -40C: 414.8k\nR at -20C: 107.5k\nR at 0C: 33.9k\nR at 35C:  6.5k
Text HLabel 3510 4030 1    50   Input ~ 0
NTCIn
$Comp
L Device:R_Potentiometer RV?
U 1 1 61093E2C
P 3150 3300
AR Path="/61044A2C/61093E2C" Ref="RV?"  Part="1" 
AR Path="/61086D02/61093E2C" Ref="RV?"  Part="1" 
AR Path="/61422789/61093E2C" Ref="RV?"  Part="1" 
AR Path="/61422AED/61093E2C" Ref="RV?"  Part="1" 
AR Path="/61424501/61093E2C" Ref="RV?"  Part="1" 
AR Path="/6142478E/61093E2C" Ref="RV10"  Part="1" 
AR Path="/6142B4BA/61093E2C" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/61093E2C" Ref="RV?"  Part="1" 
F 0 "RV10" H 3080 3346 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 3255 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 3300 50  0001 C CNN
F 3 "~" H 3150 3300 50  0001 C CNN
	1    3150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143965A
P 3150 3650
AR Path="/61044A2C/6143965A" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143965A" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143965A" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143965A" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143965A" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143965A" Ref="#PWR0120"  Part="1" 
AR Path="/6142B4BA/6143965A" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143965A" Ref="#PWR?"  Part="1" 
F 0 "#PWR0120" H 3150 3400 50  0001 C CNN
F 1 "GND" H 3155 3477 50  0000 C CNN
F 2 "" H 3150 3650 50  0001 C CNN
F 3 "" H 3150 3650 50  0001 C CNN
	1    3150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 6143965B
P 3150 3000
AR Path="/61044A2C/6143965B" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143965B" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143965B" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143965B" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143965B" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143965B" Ref="#PWR0118"  Part="1" 
AR Path="/6142B4BA/6143965B" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143965B" Ref="#PWR?"  Part="1" 
F 0 "#PWR0118" H 3150 2850 50  0001 C CNN
F 1 "+12V" H 3165 3173 50  0000 C CNN
F 2 "" H 3150 3000 50  0001 C CNN
F 3 "" H 3150 3000 50  0001 C CNN
	1    3150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3150 3150 3000
Wire Wire Line
	3150 3650 3150 3450
$Comp
L Device:R_Potentiometer RV?
U 1 1 61439653
P 3150 4800
AR Path="/61044A2C/61439653" Ref="RV?"  Part="1" 
AR Path="/61086D02/61439653" Ref="RV?"  Part="1" 
AR Path="/61422789/61439653" Ref="RV?"  Part="1" 
AR Path="/61422AED/61439653" Ref="RV?"  Part="1" 
AR Path="/61424501/61439653" Ref="RV?"  Part="1" 
AR Path="/6142478E/61439653" Ref="RV11"  Part="1" 
AR Path="/6142B4BA/61439653" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/61439653" Ref="RV?"  Part="1" 
F 0 "RV11" H 3080 4846 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 4755 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 4800 50  0001 C CNN
F 3 "~" H 3150 4800 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61439654
P 3150 5150
AR Path="/61044A2C/61439654" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439654" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439654" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439654" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439654" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439654" Ref="#PWR0127"  Part="1" 
AR Path="/6142B4BA/61439654" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439654" Ref="#PWR?"  Part="1" 
F 0 "#PWR0127" H 3150 4900 50  0001 C CNN
F 1 "GND" H 3155 4977 50  0000 C CNN
F 2 "" H 3150 5150 50  0001 C CNN
F 3 "" H 3150 5150 50  0001 C CNN
	1    3150 5150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 6143DDE3
P 3150 4500
AR Path="/61044A2C/6143DDE3" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDE3" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDE3" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDE3" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDE3" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDE3" Ref="#PWR0124"  Part="1" 
AR Path="/6142B4BA/6143DDE3" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDE3" Ref="#PWR?"  Part="1" 
F 0 "#PWR0124" H 3150 4350 50  0001 C CNN
F 1 "+12V" H 3165 4673 50  0000 C CNN
F 2 "" H 3150 4500 50  0001 C CNN
F 3 "" H 3150 4500 50  0001 C CNN
	1    3150 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4650 3150 4500
Wire Wire Line
	3150 5150 3150 4950
Wire Wire Line
	3400 4100 3510 4100
Connection ~ 5200 3400
Wire Wire Line
	5200 3850 5200 4250
Connection ~ 5200 4700
Wire Wire Line
	5200 5150 5200 5500
$Comp
L Device:C C?
U 1 1 6143DDEA
P 4390 3750
AR Path="/61044A2C/6143DDEA" Ref="C?"  Part="1" 
AR Path="/61086D02/6143DDEA" Ref="C?"  Part="1" 
AR Path="/61422789/6143DDEA" Ref="C?"  Part="1" 
AR Path="/61422AED/6143DDEA" Ref="C?"  Part="1" 
AR Path="/61424501/6143DDEA" Ref="C?"  Part="1" 
AR Path="/6142478E/6143DDEA" Ref="C33"  Part="1" 
AR Path="/6142B4BA/6143DDEA" Ref="C?"  Part="1" 
AR Path="/6142D6FB/6143DDEA" Ref="C?"  Part="1" 
F 0 "C33" H 4505 3796 50  0000 L CNN
F 1 "0.1uF" H 4505 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4428 3600 50  0001 C CNN
F 3 "~" H 4390 3750 50  0001 C CNN
	1    4390 3750
	1    0    0    -1  
$EndComp
Connection ~ 3510 4100
$Comp
L power:GND #PWR?
U 1 1 61439665
P 3510 4500
AR Path="/61044A2C/61439665" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439665" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439665" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439665" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439665" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439665" Ref="#PWR0125"  Part="1" 
AR Path="/6142B4BA/61439665" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439665" Ref="#PWR?"  Part="1" 
F 0 "#PWR0125" H 3510 4250 50  0001 C CNN
F 1 "GND" H 3515 4327 50  0000 C CNN
F 2 "" H 3510 4500 50  0001 C CNN
F 3 "" H 3510 4500 50  0001 C CNN
	1    3510 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3510 4030 3510 4100
Wire Wire Line
	4200 3500 4200 4100
$Comp
L Device:R R?
U 1 1 6142825A
P 3870 4100
AR Path="/61044A2C/6142825A" Ref="R?"  Part="1" 
AR Path="/61086D02/6142825A" Ref="R?"  Part="1" 
AR Path="/61422789/6142825A" Ref="R?"  Part="1" 
AR Path="/61422AED/6142825A" Ref="R?"  Part="1" 
AR Path="/61424501/6142825A" Ref="R?"  Part="1" 
AR Path="/6142478E/6142825A" Ref="R29"  Part="1" 
AR Path="/6142B4BA/6142825A" Ref="R?"  Part="1" 
AR Path="/6142D6FB/6142825A" Ref="R?"  Part="1" 
F 0 "R29" V 3970 4100 50  0000 C CNN
F 1 "0" V 3870 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3800 4100 50  0001 C CNN
F 3 "~" H 3870 4100 50  0001 C CNN
	1    3870 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 6142825B
P 4080 4290
AR Path="/61044A2C/6142825B" Ref="R?"  Part="1" 
AR Path="/61086D02/6142825B" Ref="R?"  Part="1" 
AR Path="/61422789/6142825B" Ref="R?"  Part="1" 
AR Path="/61422AED/6142825B" Ref="R?"  Part="1" 
AR Path="/61424501/6142825B" Ref="R?"  Part="1" 
AR Path="/6142478E/6142825B" Ref="R30"  Part="1" 
AR Path="/6142B4BA/6142825B" Ref="R?"  Part="1" 
AR Path="/6142D6FB/6142825B" Ref="R?"  Part="1" 
F 0 "R30" V 4180 4290 50  0000 C CNN
F 1 "50k" V 4080 4290 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4010 4290 50  0001 C CNN
F 3 "~" H 4080 4290 50  0001 C CNN
	1    4080 4290
	-1   0    0    1   
$EndComp
Wire Wire Line
	3510 4500 3510 4470
Wire Wire Line
	3510 4100 3720 4100
Wire Wire Line
	4020 4100 4080 4100
Connection ~ 4200 4100
Wire Wire Line
	4200 4100 4200 4600
Wire Wire Line
	4080 4140 4080 4100
Connection ~ 4080 4100
Wire Wire Line
	4080 4100 4200 4100
Wire Wire Line
	4080 4440 4080 4470
Wire Wire Line
	4080 4470 3510 4470
Wire Wire Line
	4390 3970 4390 3900
Wire Wire Line
	5200 3400 5580 3400
Wire Wire Line
	5200 4700 5580 4700
Wire Wire Line
	3300 3300 3670 3300
Wire Wire Line
	3300 4800 3630 4800
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 6142825C
P 8010 3980
AR Path="/61044A2C/6142825C" Ref="J?"  Part="1" 
AR Path="/61422789/6142825C" Ref="J?"  Part="1" 
AR Path="/61422AED/6142825C" Ref="J?"  Part="1" 
AR Path="/61424501/6142825C" Ref="J?"  Part="1" 
AR Path="/6142478E/6142825C" Ref="J14"  Part="1" 
AR Path="/6142B4BA/6142825C" Ref="J?"  Part="1" 
AR Path="/6142D6FB/6142825C" Ref="J?"  Part="1" 
F 0 "J14" H 8060 4197 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 8060 4106 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 8010 3980 50  0001 C CNN
F 3 "~" H 8010 3980 50  0001 C CNN
	1    8010 3980
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DDF0
P 8600 4140
AR Path="/61044A2C/6143DDF0" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDF0" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDF0" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDF0" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDF0" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDF0" Ref="#PWR0122"  Part="1" 
AR Path="/6142B4BA/6143DDF0" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDF0" Ref="#PWR?"  Part="1" 
F 0 "#PWR0122" H 8600 3890 50  0001 C CNN
F 1 "GND" H 8605 3967 50  0000 C CNN
F 2 "" H 8600 4140 50  0001 C CNN
F 3 "" H 8600 4140 50  0001 C CNN
	1    8600 4140
	1    0    0    -1  
$EndComp
Wire Wire Line
	8310 3980 8600 3980
Wire Wire Line
	8600 3980 8600 4080
Wire Wire Line
	8310 4080 8600 4080
Connection ~ 8600 4080
Wire Wire Line
	8600 4080 8600 4140
Text Label 3670 3130 0    50   ~ 0
TempHighSet
Text Label 3680 4930 0    50   ~ 0
TempLowSet
Wire Wire Line
	3670 3130 3670 3300
Connection ~ 3670 3300
Wire Wire Line
	3670 3300 4500 3300
Wire Wire Line
	3680 4930 3630 4930
Wire Wire Line
	3630 4930 3630 4800
Connection ~ 3630 4800
Wire Wire Line
	3630 4800 4500 4800
Text Label 7690 3980 2    50   ~ 0
TempHighSet
Text Label 7690 4080 2    50   ~ 0
TempLowSet
Wire Wire Line
	7690 3980 7810 3980
Wire Wire Line
	7810 4080 7690 4080
Text Notes 1470 5590 0    50   ~ 0
Generic input (NTCIn):\n- for resistor type: load R29 with 0 Ohm, do not load R30; R28 and NTCIn act as voltage divider\n- for voltage type: do not load R28; R29 and R30 act as voltage divider
Text HLabel 5800 3500 0    50   Output ~ 0
TempHighOut
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 61677840
P 6150 3500
AR Path="/61119591/61677840" Ref="S?"  Part="1" 
AR Path="/614C2E34/61677840" Ref="S?"  Part="1" 
AR Path="/614C4D35/61677840" Ref="S?"  Part="1" 
AR Path="/6142478E/61677840" Ref="S13"  Part="1" 
F 0 "S13" H 6270 3546 50  0000 L CNN
F 1 "SW" H 6270 3455 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 3500 50  0001 L BNN
F 3 "" H 6150 3500 50  0001 L BNN
F 4 "C&K" H 6150 3500 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 3500 50  0001 L BNN "STANDARD"
	1    6150 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61677846
P 5850 3600
AR Path="/61119591/61677846" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/61677846" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/61677846" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61677846" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 5850 3350 50  0001 C CNN
F 1 "GND" H 5855 3427 50  0000 C CNN
F 2 "" H 5850 3600 50  0001 C CNN
F 3 "" H 5850 3600 50  0001 C CNN
	1    5850 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3600 5850 3600
Wire Wire Line
	5950 3500 5800 3500
Text HLabel 5800 4800 0    50   Output ~ 0
TempLowOut
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 6167A3ED
P 6150 4800
AR Path="/61119591/6167A3ED" Ref="S?"  Part="1" 
AR Path="/614C2E34/6167A3ED" Ref="S?"  Part="1" 
AR Path="/614C4D35/6167A3ED" Ref="S?"  Part="1" 
AR Path="/6142478E/6167A3ED" Ref="S14"  Part="1" 
F 0 "S14" H 6270 4846 50  0000 L CNN
F 1 "SW" H 6270 4755 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 4800 50  0001 L BNN
F 3 "" H 6150 4800 50  0001 L BNN
F 4 "C&K" H 6150 4800 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 4800 50  0001 L BNN "STANDARD"
	1    6150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6167A3F3
P 5850 4900
AR Path="/61119591/6167A3F3" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/6167A3F3" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/6167A3F3" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6167A3F3" Ref="#PWR0126"  Part="1" 
F 0 "#PWR0126" H 5850 4650 50  0001 C CNN
F 1 "GND" H 5855 4727 50  0000 C CNN
F 2 "" H 5850 4900 50  0001 C CNN
F 3 "" H 5850 4900 50  0001 C CNN
	1    5850 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4900 5850 4900
Wire Wire Line
	5950 4800 5800 4800
$Comp
L Device:R R?
U 1 1 6147D90D
P 5580 3100
AR Path="/61044A2C/6147D90D" Ref="R?"  Part="1" 
AR Path="/61086D02/6147D90D" Ref="R?"  Part="1" 
AR Path="/61422789/6147D90D" Ref="R?"  Part="1" 
AR Path="/61422AED/6147D90D" Ref="R?"  Part="1" 
AR Path="/61424501/6147D90D" Ref="R?"  Part="1" 
AR Path="/6142478E/6147D90D" Ref="R73"  Part="1" 
AR Path="/614278B4/6147D90D" Ref="R?"  Part="1" 
F 0 "R73" V 5680 3100 50  0000 C CNN
F 1 "50k" V 5580 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5510 3100 50  0001 C CNN
F 3 "~" H 5580 3100 50  0001 C CNN
	1    5580 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5580 3250 5580 3400
Connection ~ 5580 3400
Wire Wire Line
	5580 3400 5950 3400
$Comp
L Device:R R?
U 1 1 6147F948
P 5580 4450
AR Path="/61044A2C/6147F948" Ref="R?"  Part="1" 
AR Path="/61086D02/6147F948" Ref="R?"  Part="1" 
AR Path="/61422789/6147F948" Ref="R?"  Part="1" 
AR Path="/61422AED/6147F948" Ref="R?"  Part="1" 
AR Path="/61424501/6147F948" Ref="R?"  Part="1" 
AR Path="/6142478E/6147F948" Ref="R74"  Part="1" 
AR Path="/614278B4/6147F948" Ref="R?"  Part="1" 
F 0 "R74" V 5680 4450 50  0000 C CNN
F 1 "50k" V 5580 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5510 4450 50  0001 C CNN
F 3 "~" H 5580 4450 50  0001 C CNN
	1    5580 4450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5580 4600 5580 4700
Connection ~ 5580 4700
Wire Wire Line
	5580 4700 5950 4700
$Comp
L power:GND #PWR?
U 1 1 615BDBC8
P 4390 3970
AR Path="/61044A2C/615BDBC8" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/615BDBC8" Ref="#PWR?"  Part="1" 
AR Path="/61422789/615BDBC8" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/615BDBC8" Ref="#PWR?"  Part="1" 
AR Path="/61424501/615BDBC8" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/615BDBC8" Ref="#PWR0244"  Part="1" 
AR Path="/6142B4BA/615BDBC8" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/615BDBC8" Ref="#PWR?"  Part="1" 
F 0 "#PWR0244" H 4390 3720 50  0001 C CNN
F 1 "GND" H 4395 3797 50  0000 C CNN
F 2 "" H 4390 3970 50  0001 C CNN
F 3 "" H 4390 3970 50  0001 C CNN
	1    4390 3970
	1    0    0    -1  
$EndComp
Wire Wire Line
	4390 3600 4390 3500
Connection ~ 4390 3500
Wire Wire Line
	4390 3500 4500 3500
$Comp
L power:+12V #PWR?
U 1 1 6189F471
P 5580 2650
AR Path="/61044A2C/6189F471" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6189F471" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6189F471" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6189F471" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6189F471" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6189F471" Ref="#PWR0229"  Part="1" 
AR Path="/614278B4/6189F471" Ref="#PWR?"  Part="1" 
F 0 "#PWR0229" H 5580 2500 50  0001 C CNN
F 1 "+12V" H 5595 2823 50  0000 C CNN
F 2 "" H 5580 2650 50  0001 C CNN
F 3 "" H 5580 2650 50  0001 C CNN
	1    5580 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 6189F477
P 5580 2800
AR Path="/61119591/6189F477" Ref="D?"  Part="1" 
AR Path="/614C2E34/6189F477" Ref="D?"  Part="1" 
AR Path="/614C4D35/6189F477" Ref="D?"  Part="1" 
AR Path="/61422AED/6189F477" Ref="D?"  Part="1" 
AR Path="/6142478E/6189F477" Ref="D34"  Part="1" 
F 0 "D34" V 5619 2682 50  0000 R CNN
F 1 "LED" V 5528 2682 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5580 2800 50  0001 C CNN
F 3 "~" H 5580 2800 50  0001 C CNN
	1    5580 2800
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 618A27ED
P 5580 4000
AR Path="/61044A2C/618A27ED" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618A27ED" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618A27ED" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618A27ED" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618A27ED" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618A27ED" Ref="#PWR0230"  Part="1" 
AR Path="/614278B4/618A27ED" Ref="#PWR?"  Part="1" 
F 0 "#PWR0230" H 5580 3850 50  0001 C CNN
F 1 "+12V" H 5595 4173 50  0000 C CNN
F 2 "" H 5580 4000 50  0001 C CNN
F 3 "" H 5580 4000 50  0001 C CNN
	1    5580 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618A27F3
P 5580 4150
AR Path="/61119591/618A27F3" Ref="D?"  Part="1" 
AR Path="/614C2E34/618A27F3" Ref="D?"  Part="1" 
AR Path="/614C4D35/618A27F3" Ref="D?"  Part="1" 
AR Path="/61422AED/618A27F3" Ref="D?"  Part="1" 
AR Path="/6142478E/618A27F3" Ref="D35"  Part="1" 
F 0 "D35" V 5619 4032 50  0000 R CNN
F 1 "LED" V 5528 4032 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5580 4150 50  0001 C CNN
F 3 "~" H 5580 4150 50  0001 C CNN
	1    5580 4150
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
