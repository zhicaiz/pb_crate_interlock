EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Comparator:LM393 U?
U 1 1 6143DDDB
P 4800 3400
AR Path="/61044A2C/6143DDDB" Ref="U?"  Part="1" 
AR Path="/61086D02/6143DDDB" Ref="U?"  Part="1" 
AR Path="/61422789/6143DDDB" Ref="U?"  Part="1" 
AR Path="/61422AED/6143DDDB" Ref="U?"  Part="1" 
AR Path="/61424501/6143DDDB" Ref="U?"  Part="1" 
AR Path="/6142478E/6143DDDB" Ref="U?"  Part="1" 
AR Path="/6142B4BA/6143DDDB" Ref="U?"  Part="1" 
AR Path="/6142D6FB/6143DDDB" Ref="U11"  Part="1" 
AR Path="/61439586/6143DDDB" Ref="U?"  Part="1" 
AR Path="/6143C8B2/6143DDDB" Ref="U?"  Part="1" 
F 0 "U11" H 4800 3767 50  0000 C CNN
F 1 "LM393" H 4800 3676 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 3400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 2 1 6108D921
P 4800 4700
AR Path="/61044A2C/6108D921" Ref="U?"  Part="2" 
AR Path="/61086D02/6108D921" Ref="U?"  Part="2" 
AR Path="/61422789/6108D921" Ref="U?"  Part="2" 
AR Path="/61422AED/6108D921" Ref="U?"  Part="2" 
AR Path="/61424501/6108D921" Ref="U?"  Part="2" 
AR Path="/6142478E/6108D921" Ref="U?"  Part="2" 
AR Path="/6142B4BA/6108D921" Ref="U?"  Part="2" 
AR Path="/6142D6FB/6108D921" Ref="U11"  Part="2" 
AR Path="/61439586/6108D921" Ref="U?"  Part="2" 
AR Path="/6143C8B2/6108D921" Ref="U?"  Part="2" 
F 0 "U11" H 4800 5067 50  0000 C CNN
F 1 "LM393" H 4800 4976 50  0000 C CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4800 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4800 4700 50  0001 C CNN
	2    4800 4700
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM393 U?
U 3 1 6143964F
P 4650 1950
AR Path="/61044A2C/6143964F" Ref="U?"  Part="3" 
AR Path="/61086D02/6143964F" Ref="U?"  Part="3" 
AR Path="/61422789/6143964F" Ref="U?"  Part="3" 
AR Path="/61422AED/6143964F" Ref="U?"  Part="3" 
AR Path="/61424501/6143964F" Ref="U?"  Part="3" 
AR Path="/6142478E/6143964F" Ref="U?"  Part="3" 
AR Path="/6142B4BA/6143964F" Ref="U?"  Part="3" 
AR Path="/6142D6FB/6143964F" Ref="U11"  Part="3" 
AR Path="/61439586/6143964F" Ref="U?"  Part="3" 
AR Path="/6143C8B2/6143964F" Ref="U?"  Part="3" 
F 0 "U11" H 4608 1996 50  0000 L CNN
F 1 "LM393" H 4608 1905 50  0000 L CNN
F 2 "Package_SO:SOIC-8W_5.3x5.3mm_P1.27mm" H 4650 1950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm393.pdf" H 4650 1950 50  0001 C CNN
	3    4650 1950
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 6108D923
P 5150 1500
AR Path="/61044A2C/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6108D923" Ref="#PWR0129"  Part="1" 
AR Path="/61439586/6108D923" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6108D923" Ref="#PWR?"  Part="1" 
F 0 "#PWR0129" H 5150 1350 50  0001 C CNN
F 1 "+12V" H 5165 1673 50  0000 C CNN
F 2 "" H 5150 1500 50  0001 C CNN
F 3 "" H 5150 1500 50  0001 C CNN
	1    5150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1850 4950 1850
$Comp
L power:GND #PWR?
U 1 1 6108D924
P 4100 1900
AR Path="/61044A2C/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6108D924" Ref="#PWR0131"  Part="1" 
AR Path="/61439586/6108D924" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6108D924" Ref="#PWR?"  Part="1" 
F 0 "#PWR0131" H 4100 1650 50  0001 C CNN
F 1 "GND" H 4105 1727 50  0000 C CNN
F 2 "" H 4100 1900 50  0001 C CNN
F 3 "" H 4100 1900 50  0001 C CNN
	1    4100 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1850 4100 1850
Wire Wire Line
	4100 1850 4100 1900
$Comp
L Device:C C?
U 1 1 61428254
P 5450 1850
AR Path="/61044A2C/61428254" Ref="C?"  Part="1" 
AR Path="/61086D02/61428254" Ref="C?"  Part="1" 
AR Path="/61422789/61428254" Ref="C?"  Part="1" 
AR Path="/61422AED/61428254" Ref="C?"  Part="1" 
AR Path="/61424501/61428254" Ref="C?"  Part="1" 
AR Path="/6142478E/61428254" Ref="C?"  Part="1" 
AR Path="/6142B4BA/61428254" Ref="C?"  Part="1" 
AR Path="/6142D6FB/61428254" Ref="C35"  Part="1" 
AR Path="/61439586/61428254" Ref="C?"  Part="1" 
AR Path="/6143C8B2/61428254" Ref="C?"  Part="1" 
F 0 "C35" V 5600 1850 50  0000 C CNN
F 1 "0.1uF" V 5289 1850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5488 1700 50  0001 C CNN
F 3 "~" H 5450 1850 50  0001 C CNN
	1    5450 1850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6108D927
P 5800 1850
AR Path="/61044A2C/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6108D927" Ref="#PWR0130"  Part="1" 
AR Path="/61439586/6108D927" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6108D927" Ref="#PWR?"  Part="1" 
F 0 "#PWR0130" H 5800 1600 50  0001 C CNN
F 1 "GND" H 5805 1677 50  0000 C CNN
F 2 "" H 5800 1850 50  0001 C CNN
F 3 "" H 5800 1850 50  0001 C CNN
	1    5800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1850 5600 1850
Wire Wire Line
	5300 1850 5150 1850
Connection ~ 5150 1850
$Comp
L power:+12V #PWR?
U 1 1 6142824E
P 2500 3850
AR Path="/61044A2C/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6142824E" Ref="#PWR0135"  Part="1" 
AR Path="/61439586/6142824E" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6142824E" Ref="#PWR?"  Part="1" 
F 0 "#PWR0135" H 2500 3700 50  0001 C CNN
F 1 "+12V" H 2515 4023 50  0000 C CNN
F 2 "" H 2500 3850 50  0001 C CNN
F 3 "" H 2500 3850 50  0001 C CNN
	1    2500 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6143DDE5
P 3250 4100
AR Path="/61044A2C/6143DDE5" Ref="R?"  Part="1" 
AR Path="/61086D02/6143DDE5" Ref="R?"  Part="1" 
AR Path="/61422789/6143DDE5" Ref="R?"  Part="1" 
AR Path="/61422AED/6143DDE5" Ref="R?"  Part="1" 
AR Path="/61424501/6143DDE5" Ref="R?"  Part="1" 
AR Path="/6142478E/6143DDE5" Ref="R?"  Part="1" 
AR Path="/6142B4BA/6143DDE5" Ref="R?"  Part="1" 
AR Path="/6142D6FB/6143DDE5" Ref="R31"  Part="1" 
AR Path="/61439586/6143DDE5" Ref="R?"  Part="1" 
AR Path="/6143C8B2/6143DDE5" Ref="R?"  Part="1" 
F 0 "R31" V 3350 4100 50  0000 C CNN
F 1 "50k" V 3250 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 4100 50  0001 C CNN
F 3 "~" H 3250 4100 50  0001 C CNN
	1    3250 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 3850 2500 4100
Wire Wire Line
	2500 4100 3100 4100
Wire Wire Line
	4200 3500 4385 3500
Wire Wire Line
	4500 4600 4200 4600
Wire Wire Line
	5150 1500 5150 1850
Wire Wire Line
	5100 3400 5200 3400
$Comp
L Device:C C?
U 1 1 614245CA
P 5200 3700
AR Path="/61044A2C/614245CA" Ref="C?"  Part="1" 
AR Path="/61086D02/614245CA" Ref="C?"  Part="1" 
AR Path="/61422789/614245CA" Ref="C?"  Part="1" 
AR Path="/61422AED/614245CA" Ref="C?"  Part="1" 
AR Path="/61424501/614245CA" Ref="C?"  Part="1" 
AR Path="/6142478E/614245CA" Ref="C?"  Part="1" 
AR Path="/6142B4BA/614245CA" Ref="C?"  Part="1" 
AR Path="/6142D6FB/614245CA" Ref="C36"  Part="1" 
AR Path="/61439586/614245CA" Ref="C?"  Part="1" 
AR Path="/6143C8B2/614245CA" Ref="C?"  Part="1" 
F 0 "C36" H 5315 3746 50  0000 L CNN
F 1 "15pF" H 5315 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 3550 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
	1    5200 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61428264
P 5200 4250
AR Path="/61044A2C/61428264" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61428264" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61428264" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61428264" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61428264" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61428264" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/61428264" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61428264" Ref="#PWR0137"  Part="1" 
AR Path="/61439586/61428264" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61428264" Ref="#PWR?"  Part="1" 
F 0 "#PWR0137" H 5200 4000 50  0001 C CNN
F 1 "GND" H 5205 4077 50  0000 C CNN
F 2 "" H 5200 4250 50  0001 C CNN
F 3 "" H 5200 4250 50  0001 C CNN
	1    5200 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3550 5200 3400
Wire Wire Line
	5100 4700 5200 4700
$Comp
L Device:C C?
U 1 1 6143DDE7
P 5200 5000
AR Path="/61044A2C/6143DDE7" Ref="C?"  Part="1" 
AR Path="/61086D02/6143DDE7" Ref="C?"  Part="1" 
AR Path="/61422789/6143DDE7" Ref="C?"  Part="1" 
AR Path="/61422AED/6143DDE7" Ref="C?"  Part="1" 
AR Path="/61424501/6143DDE7" Ref="C?"  Part="1" 
AR Path="/6142478E/6143DDE7" Ref="C?"  Part="1" 
AR Path="/6142B4BA/6143DDE7" Ref="C?"  Part="1" 
AR Path="/6142D6FB/6143DDE7" Ref="C38"  Part="1" 
AR Path="/61439586/6143DDE7" Ref="C?"  Part="1" 
AR Path="/6143C8B2/6143DDE7" Ref="C?"  Part="1" 
F 0 "C38" H 5315 5046 50  0000 L CNN
F 1 "15pF" H 5315 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5238 4850 50  0001 C CNN
F 3 "~" H 5200 5000 50  0001 C CNN
	1    5200 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61428261
P 5200 5500
AR Path="/61044A2C/61428261" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61428261" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61428261" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61428261" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61428261" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61428261" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/61428261" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61428261" Ref="#PWR0142"  Part="1" 
AR Path="/61439586/61428261" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61428261" Ref="#PWR?"  Part="1" 
F 0 "#PWR0142" H 5200 5250 50  0001 C CNN
F 1 "GND" H 5205 5327 50  0000 C CNN
F 2 "" H 5200 5500 50  0001 C CNN
F 3 "" H 5200 5500 50  0001 C CNN
	1    5200 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4850 5200 4700
$Comp
L Device:R_Potentiometer RV?
U 1 1 61439656
P 3150 3300
AR Path="/61044A2C/61439656" Ref="RV?"  Part="1" 
AR Path="/61086D02/61439656" Ref="RV?"  Part="1" 
AR Path="/61422789/61439656" Ref="RV?"  Part="1" 
AR Path="/61422AED/61439656" Ref="RV?"  Part="1" 
AR Path="/61424501/61439656" Ref="RV?"  Part="1" 
AR Path="/6142478E/61439656" Ref="RV?"  Part="1" 
AR Path="/6142B4BA/61439656" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/61439656" Ref="RV12"  Part="1" 
AR Path="/61439586/61439656" Ref="RV?"  Part="1" 
AR Path="/6143C8B2/61439656" Ref="RV?"  Part="1" 
F 0 "RV12" H 3080 3346 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 3255 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 3300 50  0001 C CNN
F 3 "~" H 3150 3300 50  0001 C CNN
	1    3150 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6143DDE8
P 3150 3650
AR Path="/61044A2C/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDE8" Ref="#PWR0134"  Part="1" 
AR Path="/61439586/6143DDE8" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDE8" Ref="#PWR?"  Part="1" 
F 0 "#PWR0134" H 3150 3400 50  0001 C CNN
F 1 "GND" H 3155 3477 50  0000 C CNN
F 2 "" H 3150 3650 50  0001 C CNN
F 3 "" H 3150 3650 50  0001 C CNN
	1    3150 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 6143DDE9
P 3150 3000
AR Path="/61044A2C/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6143DDE9" Ref="#PWR0132"  Part="1" 
AR Path="/61439586/6143DDE9" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6143DDE9" Ref="#PWR?"  Part="1" 
F 0 "#PWR0132" H 3150 2850 50  0001 C CNN
F 1 "+12V" H 3165 3173 50  0000 C CNN
F 2 "" H 3150 3000 50  0001 C CNN
F 3 "" H 3150 3000 50  0001 C CNN
	1    3150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3150 3150 3000
Wire Wire Line
	3150 3650 3150 3450
$Comp
L Device:R_Potentiometer RV?
U 1 1 6108D934
P 3150 4800
AR Path="/61044A2C/6108D934" Ref="RV?"  Part="1" 
AR Path="/61086D02/6108D934" Ref="RV?"  Part="1" 
AR Path="/61422789/6108D934" Ref="RV?"  Part="1" 
AR Path="/61422AED/6108D934" Ref="RV?"  Part="1" 
AR Path="/61424501/6108D934" Ref="RV?"  Part="1" 
AR Path="/6142478E/6108D934" Ref="RV?"  Part="1" 
AR Path="/6142B4BA/6108D934" Ref="RV?"  Part="1" 
AR Path="/6142D6FB/6108D934" Ref="RV13"  Part="1" 
AR Path="/61439586/6108D934" Ref="RV?"  Part="1" 
AR Path="/6143C8B2/6108D934" Ref="RV?"  Part="1" 
F 0 "RV13" H 3080 4846 50  0000 R CNN
F 1 "R_Potentiometer" H 3080 4755 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3296Y_Vertical" H 3150 4800 50  0001 C CNN
F 3 "~" H 3150 4800 50  0001 C CNN
	1    3150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6108D935
P 3150 5150
AR Path="/61044A2C/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/61422789/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/61424501/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6108D935" Ref="#PWR0141"  Part="1" 
AR Path="/61439586/6108D935" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/6108D935" Ref="#PWR?"  Part="1" 
F 0 "#PWR0141" H 3150 4900 50  0001 C CNN
F 1 "GND" H 3155 4977 50  0000 C CNN
F 2 "" H 3150 5150 50  0001 C CNN
F 3 "" H 3150 5150 50  0001 C CNN
	1    3150 5150
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 61439655
P 3150 4500
AR Path="/61044A2C/61439655" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439655" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439655" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439655" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439655" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439655" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/61439655" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439655" Ref="#PWR0138"  Part="1" 
AR Path="/61439586/61439655" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61439655" Ref="#PWR?"  Part="1" 
F 0 "#PWR0138" H 3150 4350 50  0001 C CNN
F 1 "+12V" H 3165 4673 50  0000 C CNN
F 2 "" H 3150 4500 50  0001 C CNN
F 3 "" H 3150 4500 50  0001 C CNN
	1    3150 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4650 3150 4500
Wire Wire Line
	3150 5150 3150 4950
Wire Wire Line
	3400 4100 3510 4100
Connection ~ 5200 3400
Wire Wire Line
	5200 3850 5200 4250
Connection ~ 5200 4700
Wire Wire Line
	5200 5150 5200 5500
$Comp
L Device:C C?
U 1 1 6143965C
P 4385 3830
AR Path="/61044A2C/6143965C" Ref="C?"  Part="1" 
AR Path="/61086D02/6143965C" Ref="C?"  Part="1" 
AR Path="/61422789/6143965C" Ref="C?"  Part="1" 
AR Path="/61422AED/6143965C" Ref="C?"  Part="1" 
AR Path="/61424501/6143965C" Ref="C?"  Part="1" 
AR Path="/6142478E/6143965C" Ref="C?"  Part="1" 
AR Path="/6142B4BA/6143965C" Ref="C?"  Part="1" 
AR Path="/6142D6FB/6143965C" Ref="C37"  Part="1" 
AR Path="/61439586/6143965C" Ref="C?"  Part="1" 
AR Path="/6143C8B2/6143965C" Ref="C?"  Part="1" 
F 0 "C37" H 4500 3876 50  0000 L CNN
F 1 "0.1uF" H 4500 3785 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4423 3680 50  0001 C CNN
F 3 "~" H 4385 3830 50  0001 C CNN
	1    4385 3830
	1    0    0    -1  
$EndComp
Connection ~ 3510 4100
$Comp
L power:GND #PWR?
U 1 1 61428266
P 3510 4500
AR Path="/61044A2C/61428266" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61428266" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61428266" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61428266" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61428266" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61428266" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/61428266" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61428266" Ref="#PWR0139"  Part="1" 
AR Path="/61439586/61428266" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61428266" Ref="#PWR?"  Part="1" 
F 0 "#PWR0139" H 3510 4250 50  0001 C CNN
F 1 "GND" H 3515 4327 50  0000 C CNN
F 2 "" H 3510 4500 50  0001 C CNN
F 3 "" H 3510 4500 50  0001 C CNN
	1    3510 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3510 4030 3510 4100
Wire Wire Line
	4200 3500 4200 4100
$Comp
L Device:R R?
U 1 1 6143965F
P 3870 4100
AR Path="/61044A2C/6143965F" Ref="R?"  Part="1" 
AR Path="/61086D02/6143965F" Ref="R?"  Part="1" 
AR Path="/61422789/6143965F" Ref="R?"  Part="1" 
AR Path="/61422AED/6143965F" Ref="R?"  Part="1" 
AR Path="/61424501/6143965F" Ref="R?"  Part="1" 
AR Path="/6142478E/6143965F" Ref="R?"  Part="1" 
AR Path="/6142B4BA/6143965F" Ref="R?"  Part="1" 
AR Path="/6142D6FB/6143965F" Ref="R32"  Part="1" 
AR Path="/61439586/6143965F" Ref="R?"  Part="1" 
AR Path="/6143C8B2/6143965F" Ref="R?"  Part="1" 
F 0 "R32" V 3970 4100 50  0000 C CNN
F 1 "50k" V 3870 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3800 4100 50  0001 C CNN
F 3 "~" H 3870 4100 50  0001 C CNN
	1    3870 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 61439660
P 4080 4290
AR Path="/61044A2C/61439660" Ref="R?"  Part="1" 
AR Path="/61086D02/61439660" Ref="R?"  Part="1" 
AR Path="/61422789/61439660" Ref="R?"  Part="1" 
AR Path="/61422AED/61439660" Ref="R?"  Part="1" 
AR Path="/61424501/61439660" Ref="R?"  Part="1" 
AR Path="/6142478E/61439660" Ref="R?"  Part="1" 
AR Path="/6142B4BA/61439660" Ref="R?"  Part="1" 
AR Path="/6142D6FB/61439660" Ref="R33"  Part="1" 
AR Path="/61439586/61439660" Ref="R?"  Part="1" 
AR Path="/6143C8B2/61439660" Ref="R?"  Part="1" 
F 0 "R33" V 4180 4290 50  0000 C CNN
F 1 "50k" V 4080 4290 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4010 4290 50  0001 C CNN
F 3 "~" H 4080 4290 50  0001 C CNN
	1    4080 4290
	-1   0    0    1   
$EndComp
Wire Wire Line
	3510 4500 3510 4470
Wire Wire Line
	3510 4100 3720 4100
Wire Wire Line
	4020 4100 4080 4100
Connection ~ 4200 4100
Wire Wire Line
	4200 4100 4200 4600
Wire Wire Line
	4080 4140 4080 4100
Connection ~ 4080 4100
Wire Wire Line
	4080 4100 4200 4100
Wire Wire Line
	4080 4440 4080 4470
Wire Wire Line
	4080 4470 3510 4470
Wire Wire Line
	4385 4050 4385 3980
Wire Wire Line
	5200 3400 5600 3400
Wire Wire Line
	3300 3300 3670 3300
Wire Wire Line
	3300 4800 3630 4800
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J?
U 1 1 61439661
P 8010 3980
AR Path="/61044A2C/61439661" Ref="J?"  Part="1" 
AR Path="/61422789/61439661" Ref="J?"  Part="1" 
AR Path="/61422AED/61439661" Ref="J?"  Part="1" 
AR Path="/61424501/61439661" Ref="J?"  Part="1" 
AR Path="/6142478E/61439661" Ref="J?"  Part="1" 
AR Path="/6142B4BA/61439661" Ref="J?"  Part="1" 
AR Path="/6142D6FB/61439661" Ref="J15"  Part="1" 
AR Path="/61439586/61439661" Ref="J?"  Part="1" 
AR Path="/6143C8B2/61439661" Ref="J?"  Part="1" 
F 0 "J15" H 8060 4197 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 8060 4106 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical_SMD" H 8010 3980 50  0001 C CNN
F 3 "~" H 8010 3980 50  0001 C CNN
	1    8010 3980
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61439662
P 8600 4140
AR Path="/61044A2C/61439662" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/61439662" Ref="#PWR?"  Part="1" 
AR Path="/61422789/61439662" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/61439662" Ref="#PWR?"  Part="1" 
AR Path="/61424501/61439662" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/61439662" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/61439662" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/61439662" Ref="#PWR0136"  Part="1" 
AR Path="/61439586/61439662" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/61439662" Ref="#PWR?"  Part="1" 
F 0 "#PWR0136" H 8600 3890 50  0001 C CNN
F 1 "GND" H 8605 3967 50  0000 C CNN
F 2 "" H 8600 4140 50  0001 C CNN
F 3 "" H 8600 4140 50  0001 C CNN
	1    8600 4140
	1    0    0    -1  
$EndComp
Wire Wire Line
	8310 3980 8600 3980
Wire Wire Line
	8600 3980 8600 4080
Wire Wire Line
	8310 4080 8600 4080
Connection ~ 8600 4080
Wire Wire Line
	8600 4080 8600 4140
Text Label 3670 3130 0    50   ~ 0
HumLowSet
Text Label 3680 4930 0    50   ~ 0
HumHighSet
Wire Wire Line
	3670 3130 3670 3300
Connection ~ 3670 3300
Wire Wire Line
	3670 3300 4500 3300
Wire Wire Line
	3680 4930 3630 4930
Wire Wire Line
	3630 4930 3630 4800
Connection ~ 3630 4800
Wire Wire Line
	3630 4800 4500 4800
Text Label 7690 3980 2    50   ~ 0
HumLowSet
Text Label 7690 4080 2    50   ~ 0
HumHighSet
Wire Wire Line
	7690 3980 7810 3980
Wire Wire Line
	7810 4080 7690 4080
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 6167F4E1
P 6150 3500
AR Path="/61119591/6167F4E1" Ref="S?"  Part="1" 
AR Path="/614C2E34/6167F4E1" Ref="S?"  Part="1" 
AR Path="/614C4D35/6167F4E1" Ref="S?"  Part="1" 
AR Path="/6142D6FB/6167F4E1" Ref="S15"  Part="1" 
F 0 "S15" H 6270 3546 50  0000 L CNN
F 1 "SW" H 6270 3455 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 3500 50  0001 L BNN
F 3 "" H 6150 3500 50  0001 L BNN
F 4 "C&K" H 6150 3500 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 3500 50  0001 L BNN "STANDARD"
	1    6150 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6167F4E7
P 5850 3600
AR Path="/61119591/6167F4E7" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/6167F4E7" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/6167F4E7" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6167F4E7" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 5850 3350 50  0001 C CNN
F 1 "GND" H 5855 3427 50  0000 C CNN
F 2 "" H 5850 3600 50  0001 C CNN
F 3 "" H 5850 3600 50  0001 C CNN
	1    5850 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3600 5850 3600
Wire Wire Line
	5950 3500 5800 3500
$Comp
L interlock:OS102011MS2QN1 S?
U 1 1 61681539
P 6150 4800
AR Path="/61119591/61681539" Ref="S?"  Part="1" 
AR Path="/614C2E34/61681539" Ref="S?"  Part="1" 
AR Path="/614C4D35/61681539" Ref="S?"  Part="1" 
AR Path="/6142D6FB/61681539" Ref="S16"  Part="1" 
F 0 "S16" H 6270 4846 50  0000 L CNN
F 1 "SW" H 6270 4755 50  0000 L CNN
F 2 "interlock:SW_OS102011MS2QN1" H 6150 4800 50  0001 L BNN
F 3 "" H 6150 4800 50  0001 L BNN
F 4 "C&K" H 6150 4800 50  0001 L BNN "MANUFACTURER"
F 5 "MANUFACTURER RECOMMENDATIONS" H 6150 4800 50  0001 L BNN "STANDARD"
	1    6150 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6168153F
P 5850 4900
AR Path="/61119591/6168153F" Ref="#PWR?"  Part="1" 
AR Path="/614C2E34/6168153F" Ref="#PWR?"  Part="1" 
AR Path="/614C4D35/6168153F" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/6168153F" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 5850 4650 50  0001 C CNN
F 1 "GND" H 5855 4727 50  0000 C CNN
F 2 "" H 5850 4900 50  0001 C CNN
F 3 "" H 5850 4900 50  0001 C CNN
	1    5850 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4900 5850 4900
Wire Wire Line
	5950 4800 5800 4800
Text HLabel 5800 4800 0    50   Output ~ 0
HumHighOut
Text HLabel 5800 3500 0    50   Output ~ 0
HumLowOut
Text HLabel 3510 4030 1    50   Input ~ 0
HumIn
Text Notes 1470 5590 0    50   ~ 0
Generic input (HumIn):\n- for resistor type: load R32 with 0 Ohm, do not load R33; R31 and NTCIn act as voltage divider\n- for voltage type: do not load R31; R32 and R33 act as voltage divider
Text Notes 1060 4260 0    50   ~ 0
Use HIH-4021-003 for humidity sensor
$Comp
L Device:R R?
U 1 1 61483A67
P 5600 3140
AR Path="/61044A2C/61483A67" Ref="R?"  Part="1" 
AR Path="/61086D02/61483A67" Ref="R?"  Part="1" 
AR Path="/61422789/61483A67" Ref="R?"  Part="1" 
AR Path="/61422AED/61483A67" Ref="R?"  Part="1" 
AR Path="/61424501/61483A67" Ref="R?"  Part="1" 
AR Path="/6142478E/61483A67" Ref="R?"  Part="1" 
AR Path="/614278B4/61483A67" Ref="R?"  Part="1" 
AR Path="/6142D6FB/61483A67" Ref="R75"  Part="1" 
F 0 "R75" V 5700 3140 50  0000 C CNN
F 1 "50k" V 5600 3140 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5530 3140 50  0001 C CNN
F 3 "~" H 5600 3140 50  0001 C CNN
	1    5600 3140
	-1   0    0    1   
$EndComp
Wire Wire Line
	5600 3290 5600 3400
Connection ~ 5600 3400
Wire Wire Line
	5600 3400 5950 3400
$Comp
L Device:R R?
U 1 1 61485A0F
P 5580 4440
AR Path="/61044A2C/61485A0F" Ref="R?"  Part="1" 
AR Path="/61086D02/61485A0F" Ref="R?"  Part="1" 
AR Path="/61422789/61485A0F" Ref="R?"  Part="1" 
AR Path="/61422AED/61485A0F" Ref="R?"  Part="1" 
AR Path="/61424501/61485A0F" Ref="R?"  Part="1" 
AR Path="/6142478E/61485A0F" Ref="R?"  Part="1" 
AR Path="/614278B4/61485A0F" Ref="R?"  Part="1" 
AR Path="/6142D6FB/61485A0F" Ref="R76"  Part="1" 
F 0 "R76" V 5680 4440 50  0000 C CNN
F 1 "50k" V 5580 4440 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5510 4440 50  0001 C CNN
F 3 "~" H 5580 4440 50  0001 C CNN
	1    5580 4440
	-1   0    0    1   
$EndComp
Wire Wire Line
	5580 4590 5580 4700
Wire Wire Line
	5200 4700 5580 4700
Connection ~ 5580 4700
Wire Wire Line
	5580 4700 5950 4700
$Comp
L power:GND #PWR?
U 1 1 615C3EF5
P 4385 4050
AR Path="/61044A2C/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/61422789/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/61424501/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/6142B4BA/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/615C3EF5" Ref="#PWR0245"  Part="1" 
AR Path="/61439586/615C3EF5" Ref="#PWR?"  Part="1" 
AR Path="/6143C8B2/615C3EF5" Ref="#PWR?"  Part="1" 
F 0 "#PWR0245" H 4385 3800 50  0001 C CNN
F 1 "GND" H 4390 3877 50  0000 C CNN
F 2 "" H 4385 4050 50  0001 C CNN
F 3 "" H 4385 4050 50  0001 C CNN
	1    4385 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4385 3680 4385 3500
Connection ~ 4385 3500
Wire Wire Line
	4385 3500 4500 3500
$Comp
L power:+12V #PWR?
U 1 1 618A79DE
P 5600 2690
AR Path="/61044A2C/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/614278B4/618A79DE" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/618A79DE" Ref="#PWR0231"  Part="1" 
F 0 "#PWR0231" H 5600 2540 50  0001 C CNN
F 1 "+12V" H 5615 2863 50  0000 C CNN
F 2 "" H 5600 2690 50  0001 C CNN
F 3 "" H 5600 2690 50  0001 C CNN
	1    5600 2690
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618A79E4
P 5600 2840
AR Path="/61119591/618A79E4" Ref="D?"  Part="1" 
AR Path="/614C2E34/618A79E4" Ref="D?"  Part="1" 
AR Path="/614C4D35/618A79E4" Ref="D?"  Part="1" 
AR Path="/61422AED/618A79E4" Ref="D?"  Part="1" 
AR Path="/6142D6FB/618A79E4" Ref="D36"  Part="1" 
F 0 "D36" V 5639 2722 50  0000 R CNN
F 1 "LED" V 5548 2722 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5600 2840 50  0001 C CNN
F 3 "~" H 5600 2840 50  0001 C CNN
	1    5600 2840
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR?
U 1 1 618AA205
P 5580 3990
AR Path="/61044A2C/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/61086D02/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/61422789/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/61422AED/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/61424501/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/6142478E/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/614278B4/618AA205" Ref="#PWR?"  Part="1" 
AR Path="/6142D6FB/618AA205" Ref="#PWR0232"  Part="1" 
F 0 "#PWR0232" H 5580 3840 50  0001 C CNN
F 1 "+12V" H 5595 4163 50  0000 C CNN
F 2 "" H 5580 3990 50  0001 C CNN
F 3 "" H 5580 3990 50  0001 C CNN
	1    5580 3990
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 618AA20B
P 5580 4140
AR Path="/61119591/618AA20B" Ref="D?"  Part="1" 
AR Path="/614C2E34/618AA20B" Ref="D?"  Part="1" 
AR Path="/614C4D35/618AA20B" Ref="D?"  Part="1" 
AR Path="/61422AED/618AA20B" Ref="D?"  Part="1" 
AR Path="/6142D6FB/618AA20B" Ref="D37"  Part="1" 
F 0 "D37" V 5619 4022 50  0000 R CNN
F 1 "LED" V 5528 4022 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5580 4140 50  0001 C CNN
F 3 "~" H 5580 4140 50  0001 C CNN
	1    5580 4140
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
